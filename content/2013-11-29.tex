% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 29.11.2013
% TeX: Henri

\renewcommand{\printfile}{2013-11-29}

\chapter{Statistische Beschreibung}

Ein Zustand im System ist beschrieben durch einen $2 \cdot d \cdot N$-dimensionalen Vektor $(p,q)$ im $6N$-dimensionalen Zustandsraum $\Gamma$.
Jedes Teilchen ist durch den Impuls $\bm{p}_i$ und die Ortskoordinate $\bm{q}_i$ beschrieben.
\[ (p,q) \equiv \{ \bm{p}_1,\dotsc,\bm{p}_N,\bm{q}_1,\dotsc,\bm{q}_N \} \in \Gamma \]
Es gilt $N \sim \num{e23}$. Die Zeitevolution ist bestimmt durch die Hamiltonfunktion $H(p,q)$. Die Dynamik liefern die  Hamiltonschen Bewegungsgleichungen, diese lauten
%
\begin{align*}
	\dot{p}_i &= - \frac{\partial H}{\partial q_i} \\
	\dot{q}_i &= \frac{\partial H}{\partial p_i}
\end{align*}

\begin{example}
	Beispiel für eine Hamiltonfunktion:
	%
	\begin{align*}
		H
		&= T + V \\
		&= \sum_{i=1}^{N} \frac{\bm{p}_i^2}{2 m} + \sum_{i=1}^{N} V_\textnormal{Trap}(\bm{q}_i) + \frac{1}{2} \sum_{i \neq j} V(\bm{q}_i - \bm{q}_j)
	\end{align*}
	%
	Das Wechselwirkungspotential ist z.\,B.\ ein Lennard-Jones-Potential.
\end{example}

Beobachtbare Größen sind zeitliche Mittelwerte
%
\begin{align*}
	\bar{\mathcal{M}}^t
	&= \lim_{T \to \infty} \frac{1}{T} \int_{0}^{T} \diff t\ \mathcal{M}\left[ (\bm{p}(t),\bm{q}(t)) \right]
\end{align*}

Da es unmöglich ist die Zeitevolution exakt auszurechnen benutzen wir einen statistischen Zugang.
%
\begin{align*}
	\varrho(p,q) \diff p^{3N} \diff q^{3N}
\end{align*}
%
ist die Wahrscheinlichkeit das System im Phasenraumvolumen $\diff p^{3N} \diff q^{3N}$ um den Punkt $(p,q)$ vorzufinden.

In der statistischen Beschreibung wird die Messgröße bestimmt durch
%
\begin{align*}
	\braket{\mathcal{M}}
	&= \frac{\int \diff p^{3N} \diff q^{3N}\ \varrho(p,q)\ \mathcal{M}(p,q)}{\int \diff p^{3N} \diff q^{3N}\ \varrho(p,q)}
\end{align*}
%
den \acct{Ensemble Mittelwert}.

Die Grundidee der statistischen Mechanik ist es $\varrho(p,q)$ zu bestimmen, sodass
%
\begin{align*}
	\bar{\mathcal{M}}^t = \braket{\mathcal{M}}
\end{align*}

Für festes Volumen $V$, Teilchenzahl $N$ und Energie $E$ (einem isolierten System) reduziert sich die Dichtefunktion zu
\[
	\varrho(p,q) =
	\begin{dcases}
		\const & E < H(p,q) < E+\Delta \\
		0 & \text{sonst}
	\end{dcases}
\]

\begin{theorem}[Ergodenhypothese] \index{Ergodenhypothese}
	Die Trajektorie $(p(t),q(t))$ durchläuft den gesamten Phasenraum gleichmäßig, d.\,h.\ jeder Punkt wird erreicht mit gleicher Wahrscheinlichkeit.
\end{theorem}

\begin{enumerate}
	\item Entwickle die Thermodynamik für verschiedene Ensembles.
	\item Verallgemeinerung für die Quantenmechanik.
	\item Kinetische Gastheorie. (Vereinfachung für $\varrho$)
\end{enumerate}

\begin{theorem}[Liouvillsches Theorem] \index{Liouvillsches Theorem}
	Für die Dichteänderung der Dichtefunktion $\varrho(q,p,t)$ in einem Volumen $\omega \in \Gamma$ gilt
	\begin{align*}
		\frac{\diff \varrho}{\diff t} = 0 &= \frac{\partial \varrho}{\partial t} + \left\{ \varrho, H \right\} \\
		&= \frac{\partial \varrho}{\partial t} +
		\begin{pmatrix}
			\dot{p} & \dot{q}
		\end{pmatrix}
		\begin{pmatrix}
			\nabla p \\ \nabla q
		\end{pmatrix}
		\varrho
	\end{align*}
\end{theorem}

\begin{proof}
	\begin{align*}
		\int_{\omega \in \Gamma} \diff\omega\ \frac{\partial \varrho}{\partial t}
		&= - \int_{\partial\omega} \diff\bm{\sigma}\ \begin{pmatrix} \dot{p} & \dot{q} \end{pmatrix} \varrho \\
		&= - \int_{\omega} \diff\omega\ \begin{pmatrix} \nabla p & \nabla q \end{pmatrix} \left[ \begin{pmatrix} \dot{p} & \dot{q} \end{pmatrix} \varrho \right] \\
		\frac{\partial \varrho}{\partial t}
		&= - \begin{pmatrix} \nabla p & \nabla q \end{pmatrix} \left[ \begin{pmatrix} \dot{p} & \dot{q} \end{pmatrix} \varrho \right] \\
		&= \sum_{i=1}^{3N} \left[ - \frac{\partial \dot{p}_i \varrho}{\partial p_i} - \frac{\partial \dot{q}_i \varrho}{\partial q_i} \right]
	\end{align*}
	%
	Aus der klassischen Mechanik ist bekannt
	%
	\begin{align*}
		\frac{\partial \dot{p}_i}{\partial p_i} &= 0 & \frac{\partial H}{\partial q_i} &= - \dot{p}_i \\
		\frac{\partial \dot{q}_i}{\partial q_i} &= 0 & \frac{\partial H}{\partial p_i} &= \dot{q}_i
	\end{align*}
	%
	damit
	%
	\begin{align*}
		\frac{\partial \varrho}{\partial t}
		&= \sum_{i=1}^{3N} \left[ - \dot{p}_i \frac{\partial \varrho}{\partial p_i} - \dot{q}_i \frac{\partial \varrho}{\partial q_i} \right] \\
		&= \sum_{i=1}^{3N} \left[ \frac{\partial H}{\partial q_i} \frac{\partial \varrho}{\partial p_i} - \frac{\partial H}{\partial p_i} \frac{\partial \varrho}{\partial q_i} \right] \\
		&= - \{ \varrho,H \}
	\end{align*}
\end{proof}

\chapter{Kinetische Gastheorie}

Wir definieren uns die Einteilchen-Verteilungsfunktion
\[ f(\bm{p},\bm{q},t) = N \int \diff^3 \bm{p}_2 \dots \diff^3 \bm{p}_N \diff^3 \bm{q}_2 \dots \diff^3 \bm{q}_N\ \varrho(\bm{p},\bm{p}_2,\dotsc,\bm{p}_N,\bm{q},\bm{q}_2,\dotsc,\bm{q}_N) \]
$f$ ist die Anzahl der Teilchen am Ort $\bm{q}$ mit Impuls $\bm{p}$.
%
\begin{align*}
	f_2(\bm{p}_1,\bm{p}_2,\bm{q}_1,\bm{q}_2,t)
	&= \frac{N(N-1)}{2} \int \diff^3 \bm{p}_3 \dots \diff^3 \bm{p}_N \diff^3 \bm{q}_3 \dots \diff^3 \bm{q}_N \\
	&\quad \varrho(\bm{p}_1,\bm{p}_2,\bm{p}_3,\dotsc,\bm{p}_N,\bm{q}_1,\bm{q}_2,\bm{q}_3,\dotsc,\bm{q}_N) \\
	&\tikz[remember picture,baseline=(X.base)]{\node[MidnightBlue] (X) {$\approx$};} f(\bm{p}_1,\bm{q}_1)\ f(\bm{p}_2,\bm{q}_2)
\end{align*}
\begin{tikzpicture}[remember picture,overlay]
	\draw[MidnightBlue,<-] (X) -- +(-1,0.1) node[text width=3cm,left] {Forderung des molekularen Chaos};
\end{tikzpicture}
Es gilt die Normierung
%
\begin{align*}
	\int \diff \bm{p} \diff \bm{q}\ f(\bm{p},\bm{q},t) = N
\end{align*}
%
In einem homogenen System ohne Stöße zwischen den Teilchen.
%
\begin{gather*}
	\int \diff \bm{p}\ f(\bm{p},\bm{q},t) = n(\bm{q},t) = \frac{N}{V} \\
	\frac{\diff f(q,p,t)}{\diff t} = \frac{\partial f}{\partial t} + \dot{\bm{q}} \frac{\partial f}{\partial \bm{q}} + \dot{\bm{p}} \frac{\partial f}{\partial \bm{p}} = 0
\end{gather*}

Es findet aber auch eine Impulsumverteilung durch Stöße statt. Wir erhalten daraus die \acct{Boltzmann-Transportgleichung}
%
\begin{align*}
	\partial_t f + \dot{\bm{q}} \frac{\partial f}{\partial \bm{q}} + \dot{\bm{p}} \frac{\partial f}{\partial \bm{p}} = \left. \frac{\partial f}{\partial t} \right|_\textnormal{Stöße}
\end{align*}

