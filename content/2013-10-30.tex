% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 30.10.2013
% TeX: Henri

\renewcommand{\printfile}{2013-10-30}

\begin{notice}[Repetition:] Carnot-Maschine

	\paragraph{reversible Prozesse}
	Für eine reversible Carnot-Maschine hängt der Wirkungsgrad nur von den Temperaturen der Wärmebäder ab, zwischen denen sie operiert.
	\[ \eta = 1 - \frac{T_2}{T_1} \]
	Dies erlaubt das Einführen einer absoluten Temperaturskala.

	Für einen reversiblen Prozess gilt
	\[ \oint \frac{\delta Q_\textrm{rev}}{T} = 0 \]
	Wir erhalten daraus eine neue Zustandsgröße: Die Entropie
	
	Für ein System im Zustand $B$ lässt sich die Entropie ausdrücken durch
	\[ \aligned
		S_B &= \int_\gamma \frac{\delta Q}{T} + S_A \\
		\diff S &= \frac{1}{T} \diff U + \frac{p}{T} \diff V \implies S(U,V)
	\endaligned \]

	\paragraph{irreversible Prozesse}
	\[
		S_B \geq S_A + \int_\gamma \frac{\delta Q}{T}
	\]
	In einem thermisch isolierten System ($\delta Q = 0$) kann die Entropie nur zunehmen
	\[ S_B \geq S_A \]
\end{notice}

\begin{example}
	Beim Versuch von Gay-Lussac öffnet man die Wand zwischen zwei getrennten Kammern und das Gas in der einen Kammer kann sich in die leere Kammer ausbreiten.

	\begin{figure}[htpb]
		\centering
		\begin{tikzpicture}
			\begin{scope}
				\draw (0,0) node[above right] {$V_1$} rectangle (2,1) node[above] {$T$};
				\draw[dashed] (1,1) -- (1,0);
			\end{scope}
			\draw (2.5,0.5) edge[->,bend left] (3.5,0.5);
			\begin{scope}[xshift=4cm]
				\draw (0,0) node[above right] {$V_2$} rectangle (2,1) node[above] {$T$};
			\end{scope}
		\end{tikzpicture}
		\caption{Beispiel für einen irreversiblen Weg.}
		\label{fig:2013-10-30-1}
	\end{figure}

	Sei $V_2 = 2 V_1$, wir wissen zudem $p V = n R T$
	%
	\begin{align*}
		\diff S &= \frac{1}{T} \diff U + \frac{p}{T} \diff V \\
		\Delta S
		&= \int_{V_1}^{V_2} \diff V \frac{p}{T} \\
		&= \int_{V_1}^{V_2} \diff V \frac{nR}{V} \\
		&= nR \log\left( \frac{V_2}{V_1} \right) \\
		&= nR \log 2
	\end{align*}
\end{example}

\section{Bedeutung der Entropie als thermodynamisches Potential}

Die am System geleistete Arbeit wird verallgemeinert durch
\[ \delta W = \sum\limits_{k} a_k \diff A_k \]
womit wir für $\diff S$ erhalten
%
\begin{equation*}
	\diff S = \frac{1}{T} \diff U - \frac{1}{T} \sum\limits_{k}^{N} a_k \diff A_k
\end{equation*}
%
wobei $a_k$ die intensive und $\diff A_k$ die extensive Größe ist.

Wir fassen nun die Entropie als Funktion der Zustandsvariablen $U,A_1,\dotsc,A_N$ auf
\[ S(U,A_1,\dotsc,A_N) \]

\begin{notice}
	Im Folgenden reduziert sich die Entropie auf
	\[ \diff S = \frac{1}{T} \diff U + \frac{p}{T} \diff V \]
	also $S = S(U,V)$.
\end{notice}

Wenn wir die Entropie als Funktion von den Variablen $U$ und $V$ kennen, können wir alle thermodynamischen Größen herleiten; insbesondere die thermische und kalorische Zustandsgleichung.

Zunächst finden wir einen Ausdruck für die Temperatur aus der Entropie
%
\begin{align*}
	\frac{1}{T} &= \left. \frac{\partial S}{\partial U} \right|_V = \frac{\partial S}{\partial U}(U,V) \\
	\implies T &= T(U,V) = \frac{1}{\frac{\partial S}{\partial U}(U,V)} \\
\intertext{auflösen nach $U$ liefert die kalorische Zustandsgleichung}
	\implies U(T,V) &
\end{align*}

\begin{example}
	Explizit durchgeführt für ein ideales Gas:
	\[ S = n C_V \log \frac{T}{T_0} + n R \log \frac{V}{V_0} + S_0 \]
	Aus der kalorischen Zustandsgleichung $U = \frac{3}{2} n R T$ erhält man
	\[ S(U,V) = n C_V \log \frac{U}{T_0 \frac{3}{2} n R} + n R \log \frac{V}{V_0} + S_0 \]
	Damit erhalten wir für $T$
	%
	\begin{align*}
		\frac{1}{T} &= \left. \frac{\partial S}{\partial U} \right|_V = n C_V \frac{1}{U} \\
		T &=  \frac{U}{n C_V} \implies U = n C_V T
	\end{align*}
\end{example}

Ein ähnliches Vorgehen eignet sich zur Ableitung der thermischen Zustandsgleichung
%
\begin{align*}
	\frac{p}{T} &= \left. \frac{\partial S}{\partial V} \right|_U \\
	p &= T \frac{\partial S}{\partial V} (U,V) = T \left. \frac{\partial S}{\partial V} \right|_U\Big( U(T,V), V \Big)
\end{align*}

Durch Umkehrung erhalten wir ein weiteres thermodynamisches Potential
%
\begin{align*}
	U(S,V) \; , \quad \diff U = T \diff S - p \diff V
\end{align*}

\section{Zusammenhang zwischen thermischer und kalorischer Zustandsgleichung}

Wir starten mit der Entropie $S(U,V)$ und der kalorischen Zustandsgleichung $U(T,V)$ und den Differentialen
%
\begin{align*}
	\diff S &= \frac{1}{T} \diff U + \frac{p}{T} \diff V \\
	\diff U &= \left. \frac{\partial U}{\partial T} \right|_V \diff T + \left. \frac{\partial U}{\partial V} \right|_T \diff V
\end{align*}
%
Wir wollen $\diff S$ ausdrücken als Funktion von $\diff T$ und $\diff V$, dazu betrachten wir $S(U(T,V),V)$
%
\begin{align*}
	\left. \frac{\partial S}{\partial T} \right|_V
	&= \left. \frac{\partial S}{\partial U} \right|_V \left. \frac{\partial U}{\partial T} \right|_V
	= \frac{1}{T} \left. \frac{\partial U}{\partial T} \right|_V \\
	\left. \frac{\partial S}{\partial V} \right|_T
	&= \left. \frac{\partial S}{\partial U} \right|_T \left. \frac{\partial U}{\partial V} \right|_T + \left. \frac{\partial S}{\partial V} \right|_U
	= \frac{1}{T} \left. \frac{\partial U}{\partial V} \right|_T + \frac{p}{T}
\end{align*}

Damit ergibt sich
%
\begin{align*}
	\diff S
	&= \left. \frac{\partial S}{\partial T} \right|_V \diff T + \left. \frac{\partial S}{\partial V} \right|_T \diff V \\
	&= \frac{1}{T} \left( \left. \frac{\partial U}{\partial T} \right|_V \diff T + \left[ \left. \frac{\partial U}{\partial V} \right|_T + p \right] \diff V \right) \\
	&= \frac{1}{T} \left. \frac{\partial U}{\partial T} \right|_V \diff T + \left[ \frac{1}{T} \left. \frac{\partial U}{\partial V} \right|_T + \frac{p}{T} \right] \diff V
\end{align*}

Es gilt ganz allgemein
%
\begin{align*}
	\frac{\partial S}{\partial V \partial T} = \frac{\partial S}{\partial T \partial V}
\end{align*}

Betrachten wir nun
%
\begin{align*}
	& \frac{\partial}{\partial V} \left( \frac{\partial S}{\partial T} \right)
	= \frac{1}{T} \frac{\partial U}{\partial V \partial T} \\
	={}& \frac{\partial}{\partial T} \left( \frac{\partial S}{\partial V} \right)
	= \frac{\partial}{\partial T} \left( \frac{1}{T} \frac{\partial U}{\partial V} + \frac{p}{T} \right)
	= - \frac{1}{T^2} \frac{\partial U}{\partial V} + \frac{1}{T} \frac{\partial U}{\partial T \partial V} + \frac{\partial p}{\partial T} \frac{1}{T} - p \frac{1}{T^2}
\end{align*}
%
Durch Gleichsetzen erhalten wir dann
%
\begin{align*}
	\frac{1}{T^2} \frac{\partial U}{\partial V} &= \frac{1}{T} \frac{\partial p}{\partial T} - \frac{p}{T^2} \\
	\Aboxed{\left. \frac{\partial U}{\partial V} \right|_T &= T \frac{\partial p}{\partial T} - p}
\end{align*}
%
Das ist der Zusammenhang zwischen der kalorischen und der thermischen Zustandsgleichung.

\begin{example}
	Für ein ideales Gas mit
	\[ p V = n R T \]
	erhält man für den Zusammenhang
	%
	\begin{align*}
		T \frac{\partial p}{\partial T} - p &= T \frac{n R}{V} - p = 0 \\
		\left. \frac{\partial U}{\partial V} \right|_T &= 0
	\end{align*}
\end{example}

\minisec{Materialkonstanten}

\begin{itemize}
	\item isotherme Kompressibilität \index{Kompressibilität !isotherm} \[ \kappa_T = - \frac{1}{V} \left. \frac{\partial V}{\partial p} \right|_T \]
	\item adiabatische Kompressibilität \index{Kompressibilität !adiabatisch} \[ \kappa_S = - \frac{1}{V} \left. \frac{\partial V}{\partial p} \right|_S \]
	\item Ausdehnungskoeffizient \index{Ausdehnungskoeffizient} \[ \alpha = \frac{1}{V} \left. \frac{\partial V}{\partial T} \right|_p \]
\end{itemize}

\minisec{Spezifische Wärme}

\begin{itemize}
	\item 
		\begin{itemalign}
			c_v = \left. \frac{\partial U}{\partial T} \right|_V
		\end{itemalign}
	\item 
		\begin{itemalign}
			c_p = \left. \frac{\partial U}{\partial T} \right|_p + p \left. \frac{\partial V}{\partial T} \right|_p
		\end{itemalign}
\end{itemize}
