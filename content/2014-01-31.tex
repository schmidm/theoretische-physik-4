% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 31.01.2014
% TeX: Henri

\renewcommand{\printfile}{2014-01-31}

\subsection{Ideales Fermi-Gas im Tieftemperatur-Limes}

Tieftemperatur-Limes bedeutet natürlich $T \to 0$. Bereits hergeleitet wurden
\begin{align*}
	\braket{n} &= \frac{N}{V} = \int \frac{\diff^3 k}{(2 \pi)^3} \frac{1}{\mathrm{e}^{\beta(\varepsilon_k - \mu)} + 1}, \\
	p &= \kB T \int \frac{\diff^3 k}{2 \pi} \log\left[ 1 + \mathrm{e}^{-\beta(\varepsilon_k - \mu)} \right].
\end{align*}

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (4,0) node[below] {$\varepsilon$};
		\draw[->] (0,-0.3) -- (0,2) node[left] {$f(\varepsilon)$};
		\draw (0.1,1) -- (-0.1,1) node[left] {$1$};
		\draw (2,0.1) -- (2,-0.1) node[below] {$\mu$};
		\draw[MidnightBlue] (0,1) -- (2,1) -- (2,0) -- (3.5,0);
		\draw[DarkOrange3] (0,1) -- (1.5,1) ..controls (2,1) and (2,0) .. node[right] {$T \neq 0$} (2.5,0) -- (3.5,0);
	\end{tikzpicture}
	\caption{Ausschmierung der Fermi-Kante für $T \neq 0$.}
	\label{fig:2014-01-31-1}
\end{figure}

Bei $T=0$ erhalten wir für die Teilchendichte:
\begin{align*}
	\braket{n}
	&= \int_{\frac{\hbar^2 k^2}{2m} \leq \mu} \frac{\diff^3 k}{(2 \pi)^3} 1 \\
	&= \int_{|k|<k_F} \frac{\diff^3 k}{(2 \pi)^3} \\
	&= \frac{4 \pi}{3} \frac{1}{(2 \pi)^3} k_F^3 \\
	&= \frac{k_F^3}{6 \pi^2}
\end{align*}
Für die Dispersionsrelation $\varepsilon_K = \frac{\hbar^2 \bm{k}^2}{2m}$ sind somit alle Impulse innerhalb einer Kugel mit Radius $k_F$ besetzt. 
Unter Benutzung der \acct{Fermi-Energie} $\varepsilon_F = \frac{\hbar^2 k_F^2}{2m}$ und des Fermi-Impulses $k_F$.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-2,0) -- (2,0) node[below] {$k$};
		\draw[->] (0,-0.3) -- (0,2) node[left] {$\frac{\hbar^2 k^2}{2 m}$};
		\draw[MidnightBlue] plot[domain=-1.4:1.4] (\x,\x*\x);
		\draw (0.1,1) -- (-0.1,1) node[above left] {$\varepsilon_F \equiv \mu$};
		\draw (1,0.1) -- (1,-0.1) node[below] {$k_F$};
		\draw[dotted] (-1,1) -| (1,0);
	\end{tikzpicture}
	\caption{Jedem $k < k_F$ kann ein Elektron zugeordnet werden.}
	\label{fig:2014-01.31-2}
\end{figure}

\begin{align*}
	p
	&= \kB T \int_{|k| < k_F} \frac{\diff^3 k}{(2 \pi)^3} \log\left[ \mathrm{e}^{-\beta(\varepsilon_k - \mu)} \right] \\
	&= \kB T \int_{|k| < k_F} \frac{\diff^3 k}{(2 \pi)^3} \left[ -\beta(\varepsilon_k - \mu) \right] \\
	&= \int_{|k| < k_F} \frac{\diff^3 k}{(2 \pi)^3} (\underbrace{\mu}_{\varepsilon_F} - \varepsilon_k) \\
	&= \underbrace{\varepsilon_F \int_{|k| < k_F} \frac{\diff^3 k}{(2 \pi)^3}}_{\varepsilon_F \cdot n} - \underbrace{\int \frac{\diff^3 k}{(2 \pi)^3} \frac{\hbar^2}{2m} k^2}_{\frac{3}{5} \varepsilon_F n} \\
	&= \frac{2}{5} \varepsilon_F n.
\end{align*}

Eine vollständige Rechnung mit der Sommerfeld-Expansion liefert für die innere Energie
\[ U = \braket{H} = V \int \frac{\diff^3 k}{(2 \pi)^3} \frac{1}{\mathrm{e}^{\beta(\varepsilon_k - \mu)}+1} \varepsilon_k = \frac{3}{5} \varepsilon_F n V \left[ 1 + \frac{5 \pi^2}{12} \left( \frac{\kB T}{\varepsilon_F} \right)^2 + \ldots \right]. \]
Außerdem
\[ C_V = N \kB \frac{\pi^2}{2} \frac{\kB T}{\varepsilon_F} \to 0 \text{ bei } T \to 0. \]
Damit folgt, dass $S=0$ bei $T=0$.

\subsection{Ideales Bose-Gas}

Für Bosonen gilt
\begin{align*}
	\sum_{n_k = 0} \left( z \mathrm{e}^{-\beta \varepsilon_k} \right)^{n_k}
	&= \sum_{n_k = 0}^{\infty} \left( z \mathrm{e}^{-\beta \varepsilon_k} \right)^{n_k} \\
	&= \frac{1}{1 - z \mathrm{e}^{-\beta \varepsilon_k}} \; , \quad z < 1 \\
	\leadsto
	\mathcal{Z}(V,T,z)
	&= \prod_k \left[ \frac{1}{1 - z \mathrm{e}^{-\beta \varepsilon_k}} \right] \\
	&= \prod_k \left[ \frac{1}{1 - \mathrm{e}^{-\beta (\varepsilon_k - \mu)}} \right].
\end{align*}
Für $\varepsilon_0 = 0$ muss $\mu < 0$ sein, damit keine Divergenz auftritt.
Druck und Teilchenzahl ergeben sich zu
\begin{align*}
	p
	&= \frac{\kB T}{V} \log \mathcal{Z} \\
	&= - \frac{\kB T}{V} \sum_k \log\left[ 1 - \mathrm{e}^{-\beta(\varepsilon_k - \mu)} \right], \\
	\braket{n}
	&= \frac{1}{V} \sum_k \frac{1}{\mathrm{e}^{\beta(\varepsilon_k - \mu)} - 1} \\
	&= \frac{2}{V} \frac{\partial}{\partial z} \log \mathcal{Z},
\intertext{in einer einzelnen Mode}
	\braket{n_k}
	&= \frac{1}{\mathrm{e}^{\beta(\varepsilon_k - \mu)} - 1}.
\end{align*}

Betrachte nun die $k = 0$ Mode für den Fall $\mu < 0$
\begin{align*}
	\braket{n_0}
	&= \frac{1}{V} \frac{1}{\mathrm{e}^{\beta(\varepsilon_k - \mu)} - 1} \to \frac{1}{V}.
\end{align*}
Dies verschwindet im thermodynamischen Limes, eine einzelne Mode spielt also keine Rolle.

Falls $\mu \to 0$ mit $\mu \sim \frac{1}{V}$
\begin{align*}
	\braket{n_0}
	&= \frac{1}{V} \frac{1}{\mathrm{e}^{\beta (-\mu)}-1} \\
	&= \frac{1}{V} \frac{1}{\mathrm{e}^{- \beta \mu}-1} \\
	&= \frac{1}{V} \frac{1}{- \beta \mu} \\
	&= \frac{1}{\beta} \frac{1}{- V \mu} \\
	&\to \const \text{ falls } \mu \sim \frac{1}{V}.
\end{align*}
In diesem Fall ist die Mode $k=0$ makroskopisch besetzt:
\[ N_0 = \braket{n_0} V \sim V. \]
Somit schreiben wir die Teilchendichte und den Druck:
\begin{align*}
	\braket{n}
	&= \int \frac{\diff^3 k}{(2 \pi)^3} \frac{1}{\mathrm{e}^{\beta(\varepsilon_k - \mu)} - 1} + \braket{n_0} \\
	&= \braket{n_0} + \frac{1}{\lambda^3} g_{3/2}(z), \\
	p
	&= - \kB T \int \frac{\diff^3 k}{(2 \pi)^3} \log\left[ 1 - \mathrm{e}^{-\beta(\varepsilon_k - \mu)} \right] \\
	&= \frac{\kB T}{\lambda^3} g_{5/2}(z),
\end{align*}
mit
\[ g_{3/2}(x) = - \frac{4}{\sqrt{\pi}} \int_0^\infty \diff x\ \frac{x^2}{1 - \frac{1}{2} \mathrm{e}^{x^2}} = \sum_{\ell = 1}^{\infty} \frac{x^\ell}{\ell^{3/2}}. \]
Damit
\[ \lambda^3 (\braket{n} - \braket{n_0}) = g_{3/2}(z). \]

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (4.5,0) coordinate (x) node[right] {$z$};
		\draw[->] (0,-0.3) -- (0,2) coordinate(y) node[left] {$g_{3/2}(z)$};
		\draw[dotted] (4,0) -- (4,2);
		\draw (4,0.1) -- (4,-0.1) node[below] {$1, \mu = 0$};
		\draw[MidnightBlue] (4,1.5) node[draw,fill,circle,inner sep=1pt,label={right:$2.612\ldots$}] {} edge[MidnightBlue,out=-90,in=2] coordinate[pos=0.2] (mid) (0,0);
		\draw[dotted] (x -| mid) |- (y |- mid) node[left] {$\braket{n}^3$};
	\end{tikzpicture}
	\caption{Graph der Funktion $g_{3/2}(z)$.}
	\label{fig:2014-01-31-3}
\end{figure}

Bei hohen Temperaturen ist $\mu < 0$, also $\braket{n_0} = 0$, daher
\[ \braket{n} \lambda^3 = g_{3/2}(z). \]
Bei $T_c$ ,mit $\braket{n} \lambda^3 = 2.612$ wird das chemische Potential null und $\braket{n_0}$ wird makroskopisch besetzt.
Für $T < T_c$ gilt
\begin{align*}
	n_c &= \braket{n_0} \neq 0 \\
	n_T &= \frac{2.612}{\lambda^3}
\intertext{mit}
	n &= n_T + n_c
\end{align*}
wobei $n_c$ die Kondensatdichte und $n_T$ die thermische Dichte ist. Bei $T=0$ sind alle Teilchen im Kondensat.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (4.5,0) node[right] {$T$};
		\draw[->] (0,-0.3) -- (0,2);
		\draw (2,0.1) -- (2,-0.1) node[below] {$T_c$};
		\draw (0.1,1.5) -- (-0.1,1.5) node[left] {$n$};
		\draw (0,1.5) edge[DarkOrange3,out=0,in=90] node[above] {$n_c$} (2,0);
		\draw (0,0) edge[MidnightBlue,out=0,in=-90] (2,1.5) (4,1.5);
		\draw[MidnightBlue] (2,1.5) -- node[above] {$n_T$} (4,1.5);
	\end{tikzpicture}
	\caption{Kondensationprozess als Funktion der beiden Teilchedichten.}
	\label{fig:2014-01-31-4}
\end{figure}

\paragraph{Druck}
\begin{itemize}
	\item Hochtemperatur-Limes $T \gg T_c$:
		\begin{align*}
			p = \kB T n \left[ 1 - \frac{n \lambda^3}{4 \sqrt{2}} + \ldots \right]
		\end{align*}
		Es zeigt sich, dass die erste Quantenkorrektur einen attraktiven Effekt hat.
	\item Tieftemperatur-Limes:
		\begin{align*}
			p \to 0
		\end{align*}
\end{itemize}
