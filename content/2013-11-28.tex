% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 28.11.2013
% TeX: Henri

\renewcommand{\printfile}{2013-11-28}

Wir lösen die Wärmeleitungsgleichung durch Fourier-Transformation.
%
\begin{align*}
	\mathcal{T}(k,t) = \frac{1}{\sqrt{2\pi}} \int_{-\infty}^{\infty} T(x,t)\ \mathrm{e}^{\mathrm{i} k x} \diff x
\end{align*}

\begin{notice}[Erinnerung:]
	Fourier-Transformation von Ableitungen.
	%
	\begin{align*}
		g(\omega)
		&= \frac{1}{\sqrt{2\pi}} \int_{-\infty}^{\infty} f(x)\ \mathrm{e}^{\mathrm{i} \omega x} \diff x \\
		g_1(\omega)
		&= \frac{1}{\sqrt{2\pi}} \int_{-\infty}^{\infty} \frac{\diff f(x)}{\diff x}\ \mathrm{e}^{\mathrm{i} \omega x} \diff x
	\intertext{durch partielle Integration finden wir}
		g_1(\omega)
		&= \left[ \frac{\mathrm{e}^{\mathrm{i} \omega x}}{\sqrt{2\pi}}f(x) \right]_{-\infty}^{\infty} - \frac{\mathrm{i} \omega}{\sqrt{2\pi}} \int_{-\infty}^{\infty} f(x)\ \mathrm{e}^{\mathrm{i} \omega x} \diff x
	\intertext{Für $x \to \pm \infty$ verschwindet $f(x)$, somit}
		g_1(\omega) &= - \mathrm{i} \omega g(\omega)
	\intertext{also für die $n$-te Ableitung gilt dann}
		g_n(\omega) &= (-\mathrm{i} \omega)^n g(\omega)
	\end{align*}
\end{notice}

Wenden wir dies an auf unsere Differentialgleichung
%
\begin{align*}
	\frac{\partial \mathcal{T}(k,t)}{\partial t} &= \frac{\kappa}{c_v} (-\mathrm{i} k)^2 \mathcal{T}(k,t) = - \frac{\kappa}{c_v} k^2 \mathcal{T}(k,t)
\intertext{Sortieren führt auf}
	\frac{\partial \mathcal{T}(k,t)}{\mathcal{T}(k,t)} &= - \frac{\kappa}{c_v} k^2 \partial t
\intertext{Integrieren liefert}
	\Aboxed{\mathcal{T}(k,t) &= C(k)\ \mathrm{e}^{-\frac{\kappa}{c_v} k^2 t}}
\end{align*}
%
wobei $C(k)$ eine Integrationskonstante ist, von $k$ abhängen kann und durch Anfangsbedingung $\mathcal{T}(k,0)=C(k)$ bestimmt werden kann, mittel Fourier-Transformation von $\mathcal{T}(x,0)$.

Der Einfachheit halber nehmen wir eine Delta-förmige Anfangsbedingung der Temperaturverteilung an.
%
\begin{align*}
	\mathcal{T}(k,0) = \frac{1}{\sqrt{2\pi}} \int_{-\infty}^{\infty} T(x,0)\ \mathrm{e}^{\mathrm{i} k x} \diff x
\end{align*}
%
wobei $T(x,0) = \delta(x)$. Um die Lösung für den Wärmefluss zu erhalten führen wir die Rücktransformation durch
%
\begin{align*}
	T(x,t)
	&= \frac{1}{\sqrt{2\pi}} \int_{-\infty}^{\infty} C\ \mathrm{e}^{-\mathrm{i} k x}\ \mathrm{e}^{-\frac{\kappa}{c_v} k^2 t} \diff k
\intertext{Quadratisches Ergänzen des Exponenten führt auf eine Gauß-Funktion, deren Fourier-Transformation bekannt ist.}
	&= \frac{C}{\sqrt{2\pi}}\ \mathrm{e}^{-\frac{c_v}{4 \kappa t} x^2} \int_{-\infty}^{\infty} \exp\left\{ -\frac{\kappa t}{c_v} \left( k + \frac{\mathrm{i} c_v}{2\kappa t} x \right)^2 \right\} \\
	\Aboxed{ T(x,t) &= \frac{C}{\sqrt{\frac{2 \kappa t}{c_v}}} \exp\left[ - \frac{c_v}{4 \kappa t} x^2 \right] }
\end{align*}
%
Dies ist die Lösung der \acct{Wärmeleitungsgleichung}.

\minisec{Wärmeleitung}

Wir haben die reine Diffusion zum Wärmetransport betrachtet und die Konvektion vernachlässigt. Es gibt also keine Strömung.
Außerdem
%
\begin{itemize}
	\item $\varrho = \const$
	\item $\diff u = \delta Q = c_v \diff T$ und $\delta W = p \diff V = 0$
\end{itemize}
%
\begin{align*}
	\frac{\partial u}{\partial t} = \frac{\partial T}{\partial t} c_v
\end{align*}

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw (0,0) rectangle (4,3);
		\begin{scope}[shift={(1,2)}]
			\draw (0.1,0) ellipse (0.6 and 0.8);
			\draw (-0.2,-0.2) rectangle ++(0.4,0.4);
			\draw[dotted] (0.2,-0.2) rectangle ++(0.4,0.4);
			\draw[dotted] (-0.2,0.2) rectangle ++(0.4,0.4);
			\node at (0,0) {$\scriptstyle T_i$};
			\node at (0.4,0) {$\scriptstyle T_j$};
			\foreach \i in {0,90,180,270} {
				\draw[->] (\i:0.15) -- (\i:0.4);
			}
		\end{scope}
	\end{tikzpicture}
	\caption{Illustration zur Wärmeleitungsgleichung.}
	\label{fig:2013-11-28-1}
\end{figure}

Im Zeitschitt $\Delta t$ ist\footnote{$\braket{i,j}$ bedeutet: Benachbrate $i$ und $j$.}
%
\begin{align*}
	\Delta Q &= \sum_{\braket{i,j}} Q_{ij}
\end{align*}
%
\begin{align*}
	\frac{\partial}{\partial t} Q
	&= \frac{\partial}{\partial t} \int_V \diff^3 r\ \varrho\ u(\bm{r},t) \\
	&= \int_V \diff^3 r\ \frac{\partial}{\partial t} \varrho\ u(\bm{r},t)
\end{align*}
%
\begin{align*}
	\frac{\partial}{\partial t} Q
	&= - \int_A \diff \bm{n}\ \bm{w} \\
	&= - \int_V \diff \bm{r} \div \bm{w}
\end{align*}
%
\begin{align*}
	\int \diff^3 r \left[ \varrho \frac{\partial}{\partial t} u(\bm{r},t) + \nabla \cdot \bm{w} \right] &= 0 \\
	\varrho \frac{\partial}{\partial t} u(\bm{r},t) + \nabla \cdot \bm{w} &= 0
\end{align*}

Linear response \enquote{schwache Treiber}:
%
\begin{itemize}
	\item \begin{itemalign}
			\bm{w} = - \kappa \nabla T
		\end{itemalign}

	\item \begin{itemalign}
			\frac{\partial}{\partial t} u &= c_v \frac{\partial}{\partial t} T
		\end{itemalign}

	\item \begin{itemalign}
			\frac{\partial}{\partial t} T &= \mathcal{D} \nabla^2 T
		\end{itemalign}
\end{itemize}

\begin{itemize}
	\item Thermoelektrische Effekte in geladenen Elektronensystemen

	\item Falls wir einen Wärmestrom $\bm{w}$ haben, so erhalten wir automatisch auch einen Entropiestrom $\bm{j}_S$
		\begin{align}
			\bm{j}_S &= \frac{\bm{w}}{T}
		\end{align}
		%
		\begin{itemize}
			\item \begin{itemalign}
					\frac{\partial U}{\partial t} &= \frac{\partial Q}{\partial t} = - \nabla \cdot \bm{w} \\
					\varrho \frac{\partial S}{\partial t} &= \frac{\varrho}{T} \frac{\partial U}{\partial t} = - \frac{1}{T} \nabla \cdot \bm{w} = - \nabla \cdot \bm{j}_S + \kappa \frac{(\nabla T)^2}{T^2} \\
					\nabla \cdot \bm{j}_S &= \nabla \cdot \frac{\bm{w}}{T} = \frac{1}{T} \nabla \cdot \bm{w} + \bm{w} \nabla\left( \frac{1}{T} \right) = \frac{1}{T} \nabla \cdot \bm{w} - \frac{1}{T^2} (\nabla T) \bm{w} = \frac{1}{T} \nabla \cdot \bm{w} + \frac{\kappa (\nabla T)^2}{T^2}
				\end{itemalign}

			\item \begin{itemalign}
					\diff U = T \diff S
				\end{itemalign}

			\item \begin{itemalign}
					\frac{\partial U}{\partial t} &= T \frac{\partial S}{\partial t} 
				\end{itemalign}
		\end{itemize}
		%
		\begin{align*}
			\varrho \frac{\partial S}{\partial t} + \nabla \cdot \bm{j}_S &= \kappa \frac{(\nabla T)^2}{T^2}
		\end{align*}
		%
		Falls $\nabla T \neq 0$ ist die Entropie nicht erhalten.
		Entropie-Quelle \[ \vartheta = \kappa \frac{(\nabla T)^2}{T^2} \leq 0 \] Entropie wird erzeugt.
		Die Entropie nimmt konstant zu und erreicht den maximalen Wert im Gleichgewicht mit $\nabla T = 0$.

\end{itemize}


