% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 08.11.2013
% TeX: Henri

\renewcommand{\printfile}{2013-11-08}

\section[Innere Energie \texorpdfstring{$U$}{U} als thermodynamisches Potential]{Innere Energie {\boldmath $U$} als thermodynamisches Potential}

Genau wie $S$ kann nun auch $U$ als thermodynamisches Potential aufgefasst werden. Also \[ U = U(S,V) \]
Damit ergibt sich
%
\begin{align*}
	\diff U &= \left.\frac{\partial U}{\partial S}\right|_V \diff S + \left.\frac{\partial U}{\partial V}\right|_S \diff V
\end{align*}
%
Nun ist die Frage, was die einzelnen Differentiale bedeuten. Betrachte zunächst
%
\begin{align*}
	\left.\frac{\partial U}{\partial S}\right|_V &= \frac{1}{\left.\frac{\partial S}{\partial U}\right|_V} = T
\end{align*}
%
Als nächstes benötigen wir zuerst
%
\begin{align*}
	\frac{\partial}{\partial V} S(U(V),V) = 0 = \left.\frac{\partial S}{\partial U}\right|_V \left.\frac{\partial V}{\partial U}\right|_S + \left.\frac{\partial S}{\partial V}\right|_U
\end{align*}
%
damit
%
\begin{align*}
	\left.\frac{\partial U}{\partial V}\right|_S &= - \frac{\left.\frac{\partial S}{\partial V}\right|_U}{\left.\frac{\partial S}{\partial U}\right|_V} = - \frac{\frac{p}{T}}{\frac{1}{T}} = - p
\end{align*}
%
Also ergibt sich
%
\begin{align*}
	\diff U &= T \diff S - p \diff V
\end{align*}

Betrachten wir Hemmungen der Entropie und der inneren Energie:
%
\begin{align*}
	S &= S(U,A_1,\dotsc,A_\ell) \\
	\diff S &= \frac{1}{T} \diff U - \frac{1}{T} \sum_{k = 1}^{\ell} a_k \diff A_k \\
	U &= U(S,A_1,\dotsc,A_\ell) \\
	\diff U &= T \diff S - \sum_{k = 1}^{\ell} a_k \diff A_k \\
\end{align*}

\begin{example}
	Beispiele für solche Hemmungen sind die Paare $(a_k,A_k)$.
	%
	\begin{align*}
		A_1 &= V \; , \quad a_1 = -p \\
		A_2 &= n \; , \quad a_2 = \mu 
	\end{align*}
\end{example}

\begin{notice}[Erinnerung zu irreversiblen Prozessen]
	Es gilt
	%
	\begin{align*}
		S_B &\geq S_A + \int_{A}^{B} \diff \gamma \; \frac{\delta Q}{T} \\
		\diff S &\geq \frac{\delta Q}{T} \\
		\diff S &\geq \frac{1}{T} \left( \diff U + p \diff V \right)
	\end{align*}
\end{notice}

Für festes $S$ und $V$ kann die innere Energie nur abnehmen \[ 0 \geq \diff U \]
Die innere Energie erfüllt ein Minimalprinzip:
%
\begin{align*}
	\diff U &\leq 0 \; , \quad \text{für festes $S$,$V$} \\
	\delta U|_{S,V} &= 0 \; , \quad \text{Gleichgewicht} \\
	\delta^2 U|_{S,V} &\geq 0 \; , \quad \text{Stabilität}
\end{align*}
%
Wenden wir dies an, so erhalten wir
%
\begin{align*}
	\frac{\partial^2 U}{\partial S^2} &= \left.\frac{\partial T}{\partial S}\right|_V \\
	\frac{\partial^2 U}{\partial V^2} &= - \left.\frac{\partial p}{\partial V}\right|_S = \frac{1}{V} \frac{1}{\kappa_S} \gg 0 \\
	\kappa_S &= - \frac{1}{V} \frac{\partial V}{\partial p}
\end{align*}

\section{Mehrkomponenten- und Mehrphasensysteme}

\paragraph{Mehrere Komponente}
In einem gemischten System gilt das Extermalprinzip. Die Komponenten haben denselben Druck und dieselbe Temperatur. Die Teilchenzahl $n_i$ ist konstant.
Die Systeme haben also unterschiedliche chemische Potentiale $\mu$.
Es gilt also
\[ \diff U = T \diff S - p \diff V + \sum_i \mu_i \diff n_i \]

\paragraph{Mehrere Phasen}
Auch hier gilt das Extermalprinzip. Die beiden Phasen haben dieselbe Temperatur, denselben Druck und dasselbe chemische Potential im Gleichgewicht.
Es gilt also
\[ \diff U = T \diff S - p \diff V + \mu \diff n \]

\chapter{Thermodynamische Potentiale}

Die Entropie $S(U,V)$ ist eine konkave Funktion in $U$ und $V$. Die innere Energie $U(S,V)$ ist konvex. Vergleiche auch Abbildung~\ref{fig:2013-11-06-2}.

Die Eigenschaft konkav/konvex erlaubt es uns aus den beiden Potentialen $S$ und $U$ mittels Legendre-Transformation neue Potentiale zu definieren.

\section{Legendre-Transformation}

Für eine konvexe Funktion $f(x)$ ist die \acct{Legendre-Transformation} definiert als
\[ \mathcal{L}[f(x)] = g(y) = \sup\limits_x [ xy - f(x) ] \]
Falls $f(x)$ differenzierbar ist gilt
\[ \frac{\partial}{\partial x} [xy - f(x)] = 0 \; , \quad y = f'(x) \]

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-3,0) -- (3,0) node[below] {$U$};
		\draw[->] (0,-2) -- (0,3) node[left] {$S(U,V)$};
		\draw[MidnightBlue] plot[domain=-3:3*ln(2)] (\x,{0.5*exp(\x)-1}) node[right] {$f(x)$};
		\draw[DarkOrange3] (-2,-2) -- (2,2) node[right] {$xy$};
		\draw[dotted] (0,{-ln(2)}) -- +(2,2);
		\draw[dotted] (0,{-ln(2)}) -- +(-1,-1);
		\draw ({ln(2)},0) -- ({ln(2)},{ln(2)});
	\end{tikzpicture}
	\caption{Illustration zur Legendre-Transformation}
	\label{fig:2013-11-08-1}
\end{figure}

Falls $f(x)$ differenzierbar und streng konvex ist ($f''(x) > 0$) so gilt
\[ \mathcal{L} \mathcal{L} f(x) = f(x) = \mathcal{L} h(y) \]

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\begin{scope}
			\draw[->] (-0.3,0) -- (2,0) node[below] {$x$};
			\draw[->] (0,-0.3) -- (0,2) node[left] {$f(x)$};
			\draw (1,0.1) -- (1,-0.1) node[below] {$T$};
			\draw (-0.3,0.5) .. controls (0.5,0.6) .. (1,0.8) node[dot] {} -- (2,2);
		\end{scope}
		\draw (2.5,1) edge[->,bend left] node[above] {$\mathcal{L}$} (3.5,1);
		\begin{scope}[shift={(4,0)}]
			\draw[->] (-0.3,0) -- (3,0) node[below] {$y$};
			\draw[->] (0,-0.3) -- (0,2) node[left] {$h(y)$};
			\draw (1,0.1) -- (1,-0.1) node[below] {$y_0$};
			\draw (2,0.1) -- (2,-0.1) node[below] {$y_1$};
			\draw (-0.3,-0.5) .. controls (0.5,-0.3) .. (1,0) node[dot] {} -- (2,1) node[dot] {} .. controls (2.3,1.3) .. (2.5,2);
		\end{scope}
	\end{tikzpicture}
	\caption{Phasenübergänge sind durch einen Knick bestimmt.}
	\label{fig:2013-11-08-2}
\end{figure}

\section{Freie Energie} \index{Freie Energie}

Wir starten mit der inneren Energie $U(S,V,n)$ und führen eine Legendre-Transformation in der Variable $S$ durch.
%
\begin{align*}
	F(T,V,n)
	&= \left[ U - \left.\frac{\partial U}{\partial S}\right|_{V,n} S \right] = [U - TS]
\end{align*}
%
Das totale Differential von $F$ lautet
%
\begin{align*}
	\diff F
	&= \diff U - \diff (TS) =
	\tikz[baseline=(X.base)]{\node[black] (X) {$T \diff S$};\draw (X.south west) -- (X.north east);}
	- p \diff V -
	\tikz[baseline=(X.base)]{\node[black] (X) {$T \diff S$};\draw (X.south west) -- (X.north east);}
	- S \diff T \\
	&= - p \diff V - S \diff T + \mu \diff n
\end{align*}

Wir erhalten also
%
\begin{itemize}
	\item 
		\begin{itemalign}
			\left.\frac{\partial F}{\partial T}\right|_{V,n} = - S
		\end{itemalign}
	\item 
		\begin{itemalign}
			\left.\frac{\partial F}{\partial V}\right|_{T,n} = - p
		\end{itemalign}
	\item 
		\begin{itemalign}
			\left.\frac{\partial F}{\partial n}\right|_{T,V} = \mu
		\end{itemalign}
\end{itemize}

Damit folgt sofort
%
\begin{align*}
	\frac{\partial}{\partial V} \frac{\partial F}{\partial T}
	&= - \frac{\partial S}{\partial V} = \frac{\partial}{\partial T} \frac{\partial F}{\partial V} = - \frac{\partial p}{\partial T}
\end{align*}
%
Dies ist ein Beispiel für eine \acct{Maxwell-Relation}.

$F$ ist konvex in den extensiven Variablen $V$ und $n$ aber konkav in der intensiven Größe $T$.
%
\begin{align*}
	\diff F &\leq 0 \; , \quad \text{für Hemmungen in $V$ und $n$} \\
	\delta F &= 0 \; , \quad \text{Gleichgewicht} \\
	\delta^2 F &> 0 \; , \quad \text{Stabilität}
\end{align*}
%
Betrachten wir nun wieder das Extermalprinzip:
%
\begin{align*}
	T \diff S
	&\geq \diff U + p \diff V \\
	(\diff U - T \diff S)|_{T,V} &\leq 0 \\
	\diff F|_{T,V} &= \diff(U - TS) = \diff U - T \diff S \leq 0
\end{align*}
