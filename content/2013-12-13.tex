% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 13.12.2013
% TeX: Henri

\renewcommand{\printfile}{2013-12-13}

Wir sind interessiert an Lösungen der Boltzmann-Gleichung
%
\begin{align*}
	\mathcal{D} f(\bm{r},\bm{p},t) = \frac{\partial}{\partial t} f + \bm{v} \nabla_{\bm{r}} f + \bm{F} \nabla_\bm{p} f = \left. \frac{\partial f}{\partial t} \right|_\textnormal{Stöße}
\end{align*}
%
insbesondere eine Lösung nahe beim lokalen Gleichgewicht
%
\begin{align*}
	f &= f_{\ell 0}(1 + \psi)
\intertext{mit}
	f_ {\ell 0} &= n(\bm{r},t) \frac{1}{[2 \pi m \kB T(\bm{r},t)]^{3/2}} \exp\left[ - \frac{(\bm{p} - \bm{p}_0(\bm{r},t))^2}{2 m \kB T(\bm{r},t)} \right]
\end{align*}

\begin{itemize}
	\item 
		\begin{itemalign}
			\left. \frac{\partial f}{\partial t} \right|_\textnormal{Stöße} = 0
		\end{itemalign}
	\item 
		\begin{itemalign}
			\mathcal{D} f_{\ell 0} \neq 0
		\end{itemalign}
\end{itemize}

Einsetzen von $f = f_{\ell 0} (1 + \psi)$ in den Stoßterm liefert:
%
\begin{align*}
	\left. \frac{\partial f}{\partial t} \right|_\textnormal{Stöße} &= f_{\ell 0} L \psi
\intertext{mit}
	L \psi &=
	\int \diff^3 p_1 \diff^3 p_1' \diff^3 p'\ w_{p',p_1';p,p_1}
	\left[ \psi(\bm{p} + \psi(\bm{p}_1) - \psi(\bm{p}') - \psi(\bm{p}_1') \right]
\end{align*}

Wir können uns ein Skalarprodukt definieren
%
\begin{align*}
	(g_1,g_2) &= \int \diff^3 p\ f_0(\bm{p}) g_1(\bm{p}) g_2(\bm{p}) \\
	(g_1,Lg_2) &= (Lg_1,g_2) \\
	(g,Lg) &\geq 0 \, , \quad \text{Alle Eigenwerte $\lambda \leq 0$} \\
	g &\sim 
	\begin{dcases}
		1 \\ \bm{p} \\ \bm{p}^2
	\end{dcases}
	\text{ hat den Eigenwert } 0
\end{align*}

\minisec{Relaxation}

Sei $\bm{F} = 0$, homogen (keine Ortsabhängigkeit)
%
\begin{align*}
	\mathcal{D} f = f_{\ell 0} \frac{\partial}{\partial t} \psi &= \left. \frac{\partial f}{\partial t} \right|_\textnormal{Stöße} = f_{\ell 0} L \psi \\
	\implies \frac{\partial}{\partial t} \psi &= L \psi
\end{align*}

Lösungsansatz: \[ \psi = \alpha(t) g(\bm{p}) \]
Einsetzen zeigt uns
%
\begin{align*}
	\dot{\alpha}(t) = - |\lambda| \alpha(t)
\end{align*}
%
wobei $\lambda$ Eigenwert von $g(\bm{p})$ zum Operator $L$ ist. Also
%
\begin{align*}
	\psi(\bm{p},t) = \mathrm{e}^{-|\lambda| t} g(\bm{p})
\end{align*}
%
Die allgemeine Lösung ist
%
\begin{align*}
	\psi(\bm{p},t) = \sum_{\lambda} \mathrm{e}^{-|\lambda| t} \alpha_\lambda g_\lambda(\bm{p})
\end{align*}

\subsection{Relaxationszeitapproximation}

Wir ersetzen das Spektrum von $L$ durch einen einzelnen Zerfallsparameter $\tau$. Wir setzen
%
\begin{align*}
	L \psi = - \frac{\psi}{\tau}
\end{align*}
%
für alle Moden, die nicht erhalten sind.

\minisec{Transport}

Die Kraft ist jetzt klein und hat eine schwach variierende Ortsabhängigkeit.
%
\begin{align*}
	\mathcal{D} f
	&= - f_{\ell 0} \frac{\psi}{\tau} = (f_{\ell 0} L \psi) \\
	&= \mathcal{D} f_{\ell 0} + \cancel{$\mathcal{D} f_{\ell 0} \psi$} = - f_{\ell 0} \frac{\psi}{\tau} \\
	\psi &= \frac{- \tau \mathcal{D} f_{\ell 0}}{f_{\ell 0}}
\end{align*}

Die Lösung der Boltzmann-Transport-Gleichung hat in erster Ordnung die Form
%
\begin{align*}
	f = f_{\ell 0} (1 + \psi) = f_{\ell 0} - \tau \mathcal{D} f_{\ell 0}
\end{align*}

Die Frage ist nun, wie klein die Störung tatsächlich ist.
%
\begin{itemize}
	\item Die Frequenzen der Störung $\omega \tau \ll 1$.
	\item Die Ortsabhägngikeit muss klein sein $k \cdot \ell \ll 1$ mit $\ell = v_T \tau$.
\end{itemize}

\chapter{Hydrodynamik}
Wir betrachten die lokale Maxwell-Boltzmann Verteilung
\begin{align*}
	f_ {\ell 0} &= n(\bm{r},t) \frac{1}{[2 \pi m \kB T(\bm{r},t)]^{3/2}} \exp\left[ - \frac{m (\bm{v} - \bm{u}_0(\bm{r},t))^2}{2 \kB T(\bm{r},t)} \right]
\intertext{mit}
	\bm{p} &= m \bm{v} \\
	\bm{p}_0(\bm{r},t) &= m \bm{u}(\bm{r},t)
\end{align*}
%
dabei ist
%
\begin{itemize}
	\item $n(\bm{r},t)$ die Teilchendichte
	\item $\bm{u}(\bm{r},t)$ die konvektive Strömung
	\item $T(\bm{r},t)$ die lokale Temperatur
\end{itemize}
%
Folglich haben wir fünf lokale Erhaltungsgrößen $\phi = 1, \bm{p}, \bm{p}^2$.
%
\begin{align*}
	\int \diff^3 p\ \phi \mathcal{D} f(\bm{r},t)
	&= \int \diff^3 p\ \phi \partial_t f |_\textnormal{Stöße} = 0 \\
	&= \frac{\partial}{\partial t} \int \diff^3 p\ \phi f(\bm{r},t)
	+ \nabla_\bm{r} \int \diff^3 p\ \bm{v} f \phi
	- \bm{F} \int \diff^3 p\ (\nabla_\bm{p} \phi) f = 0
\end{align*}

Betrachte das Verhalten unter den Erhaltungsgrößen.
%
\begin{description}
	\item[$\phi = 1$]
		Kontinuitätsgleichung
		\begin{align*}
			\partial_t n + \partial_i j_i = 0
		\end{align*}
	\item[$\phi = \bm{p}$]
		Impulserhaltung
		\begin{align*}
			m \partial_t j_k + \partial_i \Pi_{ik} = m F_k
		\end{align*}
	\item[$\phi = \bm{p}^2$]
		Energieerhaltung
		\begin{align*}
			\partial_t e + \partial_t \varepsilon_i = j_i F_i
		\end{align*}
\end{description}
%
wobei
%
\begin{itemize}
	\item Teilchendichte
		\begin{itemalign}
			n = \int \diff^3 p\ f
		\end{itemalign}
	\item Impulsdichte
		\begin{itemalign}
			j_k = \int \diff^3 p\ v_k f
		\end{itemalign}
	\item Energiedichte
		\begin{itemalign}
			e = \int \diff^3 p\ \frac{p^2}{2 m} f
		\end{itemalign}
	\item Impulsstromdichte
		\begin{itemalign}
			\Pi_{ik} = m \int \diff^3 p\ v_i v_k f
		\end{itemalign}
	\item Energiestromdichte
		\begin{itemalign}
			\varepsilon_i = \int \diff^3 p\ \frac{p^2}{2 m} v_i f
		\end{itemalign}
\end{itemize}

Wir können jetzt unsere Lösung der Boltzmann-Transport-Gleichung
%
\begin{align*}
	f = f_{\ell 0} - \tau \mathcal{D} f_{\ell 0}
\end{align*}
%
in unsere Erhaltungssätze einsetzen.
%
\begin{description}
	\item[Nullte Ordnung] Euler Gleichungen
	\item[Erste Ordnung] mit Korrektur $- \tau \mathcal{D} f_{\ell 0}$. Navier-Stokes-Gleichungen.
\end{description}

\section{Nullte Ordnung, Euler Gleichungen}
\index{Euler Gleichungen}
%
Wir separieren die Ausdrücke in der Energiestromdichte und erhalten somit:
%
\begin{align*}
	f &= \frac{n}{(2 \pi \kB T)^{3/2}} \exp\left[ - \frac{m (v - u)^2}{2 \kB T} \right] \\
	n(\bm{r},t) &= \int \diff^3 p\ f_{\ell 0} \equiv n(\bm{r},t) \\
	j_i(\bm{r},t) &= n(\bm{r},t) \cdot u_i(\bm{r},t) \\
	e(\bm{r},t) &= \int \diff^3 p\ \frac{p^2}{2 m} f_{\ell 0} \stackrel{p' = p - mu}{=} n \frac{m u^2}{2} + \underbrace{\frac{m}{2} \int \diff^3 p\ v^2 f|_{u=0}}_{\frac{3}{2} n \kB T(\bm{r},t)} = \frac{n m u^2}{2} + \frac{3}{2} n \kB T(\bm{r},t) \\
	\Pi_{ik} &= m \int \diff^3 p\ v_i v_k f = m n u_i u_k + \delta_{ik} p \; , \quad p = n \kB T \\
	\varepsilon_i &= \frac{m n}{2} u^2 u_i + u_i \frac{3}{2} n \kB T + u_i p + \underbrace{\frac{m}{2} \int \diff^3 p\ v^2 v_i f|_{u=0}}_{\textnormal{Wärmestrom, hier $=0$}}
\end{align*}

\begin{align*}
	\partial_t n + \partial_i (n u_i) &= 0 \\
	m \partial_t (n u_k) + \partial_i [ m n u_i u_k + \delta_{ik} n \kB T ] &= n F_k \\
	\partial_t \left( \frac{mn}{2} u^2 + \frac{3}{2} n \kB T \right) + \partial_i \left[ \left( \frac{mn}{2} u^2 + \frac{5}{2} n \kB T \right) u_i \right] &= n F_i \cdot u_i
\end{align*}

