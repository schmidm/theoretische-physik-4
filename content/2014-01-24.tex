% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 24.01.2014
% TeX: Henri

\renewcommand{\printfile}{2014-01-24}

\begin{notice}[Erinnerung:]
	Spezialfall
	%
	\begin{align*}
		\varrho &= \sum_{n} p_n \ket{n}\bra{n} \; , \quad \text{mit } H\ket{n}=E_n\ket{n}
	\end{align*}
\end{notice}

\section{Ensembles in der Quantenstatistik}

\paragraph{Mikrokanonisches Ensemble} \index{Ensemble!quantenmechanisch mikrokanonisch}
%
\begin{itemize}
	\item klassisch
		\begin{align*}
			\varrho(p,q) &=
			\begin{cases}
				\frac{1}{h^{3N} N!} &, E<H(p,q)<E+\Delta \\
				0 &, \text{sonst}
			\end{cases} \\
			\Gamma(E) &= \int \diff p \diff q\ \varrho(p,q) = \int\limits_{\mathrlap{E<H(p,q)<E+\Delta}} \frac{\diff p \diff q}{h^{3N} N!} \\
			S(U=E,V,N) &= \kB \log \Gamma(E)
		\end{align*}

	\item quantenmechanisch
		\begin{align*}
			\varrho &= \sum_{n} p_n \ket{n}\bra{n} \; , \quad 
			p_n =
			\begin{cases}
				1 &, E < E_n < E + \Delta \\
				0 &, \text{sonst}
			\end{cases} \\
			\Gamma(E) &= \tr \varrho = \omega(E) \cdot \Delta \; , \quad \omega(E)\text{: Spektrale Dichte} \\
			S(U=E,V,N) &= \kB \log \Gamma(E)
		\end{align*}
\end{itemize}

\paragraph{Kanonisches Ensemble} \index{Ensemble!quantenmechanisch kanonisch}
%
\begin{itemize}
	\item quantenmechanisch
		\begin{align*}
			\varrho &= \sum_{n} p_n \ket{n}\bra{n} = \mathrm{e}^{-\beta H} \; , \quad
			p_n = \mathrm{e}^{-\beta E_n} = \mathrm{e}^{- \frac{E_n}{\kB T}} \\
			Z_N &= \tr \varrho = \tr \mathrm{e}^{-\beta H} \\
			F(T,V,N) &= - \kB T \log\left[ \tr \mathrm{e}^{-\beta H} \right] = - \kB T \log Z_N
		\end{align*}
\end{itemize}

\begin{notice}
	\[ U(t) = \mathrm{e}^{- \frac{\mathrm{i}}{\hbar} H t} \]
	Setze die Zeit analytisch fort $\tau = - \mathrm{i} t$
	%
	\begin{align*}
		U(\tau) &= \mathrm{e}^{- \frac{H \tau}{\hbar}} \\
		U(\beta \hbar) &\equiv \varrho = \mathrm{e}^{-\beta H}
	\end{align*}
\end{notice}

\paragraph{Großkanonisches Ensemble} \index{Ensemble!quantenmechanisch großkanonisch}
%
\begin{itemize}
	\item quantenmechanisch
		\begin{align*}
			\varrho &= \mathrm{e}^{- \frac{pV}{\kB T}} \mathrm{e}^{-\beta(H-\mu N)} \\
			\mathcal{Z} &= \tr \mathrm{e}^{-\beta (H - \mu N)} = \sum_{N=0}^{\infty} z^N \tr\left[ \mathrm{e}^{-\beta H} \right]_N \\
			\Omega &= - p V = - \kB T \log \mathcal{Z}
		\end{align*}
\end{itemize}

\appendix

\chapter{Anwendungen}

\section{Freies Teilchen im kanonischen Ensemble}

Wir betrachten ein einzelnes freies Teilchen im kanonischen Ensemble.

Der Hamiltonoperator hat also die Form \[ H = \frac{p^2}{2m} \]

Zuerst rechnen wir klassich. Die Zustandssumme lautet
%
\begin{align*}
	Z_1
	&= \int \frac{\diff^3 p \diff^3 q}{h^3 1!} \mathrm{e}^{-\frac{p^2}{2 m \kB T}} \\
	&= \frac{V}{h^3} \int \diff^3 p\ \mathrm{e}^{-\frac{p^2}{2 m \kB T}} \\
	&= V \left( \frac{2 \pi m \kB T}{h^2} \right)^{3/2}
\intertext{mit der de-Broglie-Wellenlänge $\lambda_\textnormal{dB} = \sqrt{\frac{2 \pi \hbar^2}{m \kB T}}$}
	&= \frac{V}{\lambda_\textnormal{dB}^3}
\end{align*}

Nun die quantenmechanische Rechnung. Hier lautet der Hamiltonoperator
\[ H = - \frac{\hbar^2}{2m} \nabla^2 \]
Die Eigenzustände des Hamiltonoperators sind ebene Wellen mit
\[ \psi_n(\bm{x}) = \braket{\bm{x}|n} = \frac{1}{\sqrt{N}} \mathrm{e}^{\mathrm{i} \bm{k}_n \cdot \bm{x}} \]
In der stationären Schrödingergleichung ergibt sich damit
%
\begin{align*}
	H \psi_n(\bm{x})
	&= - \frac{\hbar^2}{2m} \nabla^2 \frac{1}{\sqrt{N}} \mathrm{e}^{\mathrm{i} \bm{k}_n \cdot \bm{x}} \\
	&= \underbrace{\frac{\hbar^2}{2m} \bm{k}^2}_{E_n} \frac{1}{\sqrt{N}} \mathrm{e}^{\mathrm{i} \bm{k}_n \cdot \bm{x}} \\
	&= E_n \psi_n(\bm{x})
\end{align*}
%
Es bleibt noch die Normierungskonstante $N$ zu berechnen. Dazu verwenden wir die Normierungsbedingung
%
\begin{align*}
	1 &\stackrel{!}{=} \braket{\psi_n|\psi_n} \\
	&= \int \diff \bm{x}\ \psi_n^*(\bm{x}) \psi_n(\bm{x}) \\
	&= \int \diff \bm{x}\ |\psi_n^*(\bm{x})|^2 \\
	&= \int \diff \bm{x}\ \frac{1}{N} = \frac{V}{N} \\
	\leadsto N &= V
\end{align*}
%
Damit lauten die Eigenzustände in Ortsdarstellung endgültig
\[ \psi_n(\bm{x}) = \frac{1}{\sqrt{V}} \mathrm{e}^{\mathrm{i} \bm{k}_n \cdot \bm{x}} \]
Es stellt sich noch die Frage, welche Werte für $\bm{k}_n$ erlaubt sind.

Das Quantisierungsvolumen $V$ verlangt Randbedingungen. Die Wahl der Randbedingungen spielt keine Rolle, wenn wir unser Quantisierungsvolumen nach $\infty$ schicken. Daher wählen wir die einfachsten, nämlich periodische Randbedingungen. In funktionaler Form lauten diese
\[ \psi(\bm{x} + \bm{e}_\alpha L) = \psi(\bm{x}) \; , \quad \alpha = x,y,z \]
mit $\bm{e}_\alpha$ dem Einheitsvektor in $\alpha$-Richtung. Wir setzen dies nun explizit für die $x$-Richtung ein.
%
\begin{align*}
	\psi(x + L)
	&= \mathrm{e}^{\mathrm{i} k_{x,n} (x+L)} \\
	&= \mathrm{e}^{\mathrm{i} \frac{2 \pi}{L} n_x (x+L)} \\
	&= \mathrm{e}^{\mathrm{i} \frac{2 \pi}{L} n_x x} + \mathrm{e}^{\mathrm{i} 2 \pi n_x} \\
	&= \mathrm{e}^{\mathrm{i} \frac{2 \pi}{L} n_x x} \left( \mathrm{e}^{\mathrm{i} 2 \pi} \right)^{n_x} \\
	&= \mathrm{e}^{\mathrm{i} k_{x,n} x} 1^{n_x} \\
	&= \mathrm{e}^{\mathrm{i} k_{x,n} x} = \psi(x)
\end{align*}
%
Wir erhalten damit
\[
	\bm{k}_n =
	\begin{pmatrix}
		n_x \\ n_y \\ n_z \\
	\end{pmatrix}
	2 \pi L
	\; , \quad
	n_\alpha \in \mathbb{Z}
\]

Damit ergibt sich die Zustandssumme zu
%
\begin{align*}
	Z_1
	&= \tr\left[ \mathrm{e}^{-\beta H} \right] \\
	&= \sum_{n} \mathrm{e}^{-\beta E_n} \\
	&= \sum_{n_x,n_y,n_z \in \mathbb{Z}} \mathrm{e}^{-\beta \frac{\hbar^2}{2m} \bm{k}_n^2} \\
	&= \sum_{\bm{k}} \mathrm{e}^{-\beta \frac{\hbar^2}{2m} \bm{k}^2} \\
	&= \left( \frac{L}{2 \pi} \right)^3 \sum_{n_x,n_y,n_z \in \mathbb{Z}} \left( \frac{2\pi}{L} \right)^3 \mathrm{e}^{-\beta \frac{\hbar^2}{2m} \bm{k}_n^2}
\intertext{Diese Summe entspricht einer riemannschen Summe, welche gegen das Integral konvergiert für $L \to \infty$}
	&= V \int \frac{\diff^3 k}{(2\pi)^3} \mathrm{e}^{-\beta \frac{\hbar^2 k^2}{2m}} \\
	&= V \left( \frac{m \kB T}{2 \pi \hbar^2} \right)^{3/2}
\end{align*}

Die Ergebnisse der klassischen statistischen Mechanik und der Quantenstatistik stimmen also überein.

Die de-Broglie-Wellenlänge entspricht gerade der Wellenlänge eines Teilchens bei der Energie $E = \pi \kB T$.
Die de-Broglie-Wellenlänge beschreibt gerade die Distanz auf der quantenmechanische Korrelationen noch messen können. Auf größeren Distanzen verhält sich das Teilchen klassisch. Um die Längenskala dieser Korrelationen zu ermitteln muss die folgende Größe bestimmt werden.
\[ G(x,y) = \left\langle \ket{x}\bra{y} \right\rangle = \tr\left[ \ket{x}\bra{y} \right] \mathrm{e}^{-\beta H} \]
Für eine ebene Wellen kann man dies leicht ausrechnen
%
\begin{align*}
	\tr\left[ \ket{x}\braket{y|\psi}\bra{\psi}  \right]
	&= \braket{\psi|x}\braket{y|\psi} \\
	&= \psi^*(x) \psi(y) \\
	&= \mathrm{e}^{\mathrm{i} k (x - y)} \\
	\sum_{k} \mathrm{e}^{\mathrm{i} k (x-y)} \mathrm{e}^{-\beta \frac{\hbar^2 k^2}{2m}} &\sim \mathrm{e}^{- \frac{(x-y)^2}{\lambda_\textnormal{dB}^2}}
\end{align*}
