% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 29.01.2014
% TeX: Henri

\renewcommand{\printfile}{2014-01-29}

\section{Ideale Quantengase}

\subsection{Relevante Begriffe der Quantenstatistik}

Der Hamiltonoperator für ein einzelnes Teilchen lautet
\[ H_1 = \frac{p^2}{2 m}, \]
dessen Eigenenergien gegeben sind durch
\[ E_k = \frac{\hbar^2 k^2}{2 m}, \]
mit den Eigenzuständen $\ket{k}$.

Wir sind interessiert am großkanonischen Ensemble. Dabei haben wir eine feste Temperatur $T$ durch ein Wärmebad, sowie ein chemisches Potential $\mu$, das den Teilchenaustausch regelt. Der Teilchenaustausch wird über die Zweitquantisierung des Hamiltonoperators ausgedrückt. Dabei werden die Zustände geschrieben als Besetzungszahl einer Mode. Der Hilbertraum setzt sich zusammen als direkte Summe der Ein-Teilchen-Hilberträume $\mathscr{H}_1$
\[ \mathscr{H} = \bigoplus_{N = 0}^{\infty} \left( \bigotimes_{i = 1}^{\infty} \mathscr{H}_1 \right), \]
mit der Basis
\[ \ket{\{n_k\}} = \ket{\dotsc,n_{k_1},n_{k_2},\dotsc}, \]
dabei ist $n_k$ die Anzahl der Teilchen in der Mode $k$. Die gesamte Teilchenzahl ist die Summe über die Besetzungen aller Moden
\[ N = \sum_k n_k. \]
Der Zustand ohne Teilchen wird auch Vakuum genannt und ist funktional charakterisiert durch den Zustand $\ket{0}$. Der Hilbertraum zum Vakuum
\[ (\mathscr{H}_1)^0 = \mathds{1}. \]

Betrachten wir die Systematik der Besetzungszahldarstellung
\begin{itemize}
	\item Kein Teilchen: $\ket{0}$
	\item Ein Teilchen in der Mode $k$: $\ket{1_k} \equiv \ket{k}$
	\item Zwei Teilchen, eins in der Mode $k$ eins in der Mode $q$: $\ket{1_k,1_q} \equiv \ket{k}\ket{q}$
	\item Drei Teilchen, zwei in der Mode $k$ eins in der Mode $q$: $\ket{2_k,1_q} \equiv \ket{k}\ket{k}\ket{q}$
\end{itemize}

Wir müssen unterscheiden zwischen Fermionen und Bosonen, denn für Fermionen gilt das Pauli-Ausschlussprinzip. Es dürfen sich keine zwei Fermionen im selben Zustand befinden. Daher gilt
\begin{itemize}
	\item Fermionen: $n_k = 0,1$
	\item Bosonen: $n_k = 0,1,\dotsc,\infty$
\end{itemize}

Für die Energie eines solchen Vielteilchensystems gilt
\[ E = \sum_k \frac{\hbar^2 k^2}{2 m} n_k. \]

\subsection{Großkanonisches Ensemble}

Für die Zustandssumme gilt
%\marginpar
%		\footnote{$\displaystyle\sum_{n=0}^{\infty} \sum_{m=0}^{\infty} a_n a_m = \left( \sum_{n=0}^{\infty} a_n \right) \left( \sum_{m=0}^{\infty} a_m \right) =  \left( \sum_{n=0}^{\infty} a_n \right)^2$}%
\begin{align*}
	\mathcal{Z}(V,T,z)
	&= \sum_{N=0}^{\infty} z^N Z_N(V,T) \\
	&= \sum_{N=0}^{\infty} z^N \tr \mathrm{e}^{-\beta H} \\
	&= \sum_{N=0}^{\infty} z^N \sum_{\substack{ \{n_k\} \\ \sum_k n_k = N}} \mathrm{e}^{-\beta \sum_k \varepsilon_k n_k}
\intertext{wobei $\varepsilon_k = \frac{\hbar^2 k^2}{2 m}$. Die Berechnung der großkanonischen Zustandssumme verlangt, dass zunächst für eine feste Teilchenzahl über alle damit verträglichen Zustände summiert wird und dann über alle Teilchenzahlen $n=0,1,2,\ldots$. Deshalb die Einschränkung $\sum_{k} n_k = N$. Darum folgt}
	&= \sum_{N=0}^{\infty} \sum_{\substack{ \{n_k\} \\ \sum_k n_k = N}} \mathrm{e}^{-\beta \sum_k \varepsilon_k n_k} z^{\sum_k n_k} \\
	&= \sum_{\{n_k\}} \prod_k \left( \mathrm{e}^{-\beta \varepsilon_k n_k} z^{n_k} \right) \\
	&= \sum_{\{n_k\}} \prod_k \left( \mathrm{e}^{-\beta \varepsilon_k} z \right)^{n_k}
\intertext{nun vertauschen wir Summe und Produkt}
	&= \sum_{n_1 = 0} \sum_{n_2 = 0} \sum_{n_3 = 0} \cdots \left( z \mathrm{e}^{-\beta \varepsilon_k} \right)^{n_1} \left( z \mathrm{e}^{-\beta \varepsilon_k} \right)^{n_2} \left( z \mathrm{e}^{-\beta \varepsilon_k} \right)^{n_3} \cdots \\
	&= \prod_k \left[ \sum_{n_k = 0} \left( z \mathrm{e}^{-\beta \varepsilon_k} \right)^{n_k} \right].
\end{align*}
%
\begin{notice}
	Je nachdem ob Bosonen oder Fermionen vorliegen erhalten wir für die Zustandssumme folgendes Ergebnis (genauere Herleitung später):
	%
	\begin{align}
		\mathcal{Z}(V,T,z) &= \prod_k \left[ \sum_{n_k = 0} \left( z \mathrm{e}^{-\beta \varepsilon_k} \right)^{n_k} \right] \\
		&=	\begin{cases}
				\prod_k \frac{1}{1-\mathrm{e}^{-\beta(\varepsilon_k-\mu)}} & \text{Bosonen} \\
				\prod_k \left( 1 + \mathrm{e}^{-\beta(\varepsilon_k-\mu)} \right) & \text{Fermionen} 
			\end{cases}.
	\end{align}
	%
\end{notice}
%
Für Fermionen gilt explizit
\begin{align*}
	\sum_{n_k = 0} \left( z \mathrm{e}^{-\beta \varepsilon_k} \right)^{n_k}
	&= \sum_{n_k = 0}^{1} \left( z \mathrm{e}^{-\beta \varepsilon_k} \right)^{n_k} \\
	&= 1 +  z \mathrm{e}^{-\beta \varepsilon_k} \\
	\leadsto
	\mathcal{Z}(V,T,z)
	&= \prod_k \left[ 1 +  z \mathrm{e}^{-\beta \varepsilon_k} \right].
\end{align*}
Das große Potential ist proportional zum Druck und es folgt
\begin{align*}
	p V
	&= \kB T \log \mathcal{Z}(V,T,z) \\
	&= \kB T \log \prod_k \left[ 1 +  z \mathrm{e}^{-\beta \varepsilon_k} \right] \\
	&= \kB T \sum_k \log \left[ 1 +  z \mathrm{e}^{-\beta \varepsilon_k} \right] \\
	&= \kB T V \int \frac{\diff^3 k}{(2 \pi)^3} \log \left[ 1 +  z \mathrm{e}^{-\beta \varepsilon_k} \right].
\end{align*}
Aus den großen Potential lassen sich nun alle interessierenden thermodynamischen Größen ableiten.
Der Mittelwert der Teilchenzahl kann ebenfalls aus der Zustandssumme berechnet werden
\begin{align*}
	\braket{N}
	&= z \partial_z \log \mathcal{Z}(V,T,z) \\
	&= z \partial_z V \int \frac{\diff^3 k}{(2 \pi)^3} \log \left[ 1 +  z \mathrm{e}^{-\beta \varepsilon_k} \right] \\
	&= V \int \frac{\diff^3 k}{(2 \pi)^3} \frac{z \mathrm{e}^{-\beta \varepsilon_k}}{1 +  z \mathrm{e}^{-\beta \varepsilon_k}}
\intertext{mit $z = \mathrm{e}^{\beta \mu}$}
	&= V \int \frac{\diff^3 k}{(2 \pi)^3} \frac{\mathrm{e}^{-\beta (\varepsilon_k - \mu)}}{1 +  \mathrm{e}^{-\beta (\varepsilon_k - \mu)}} \\
	&= V \int \frac{\diff^3 k}{(2 \pi)^3} \frac{1}{\mathrm{e}^{\beta (\varepsilon_k - \mu)} + 1}.
\end{align*}
Als Verteilungsfunktion ergibt sich also die \acct{Fermi-Dirac-Statistik}
\begin{align*}
	\braket{n_k} &= \frac{1}{\mathrm{e}^{\beta (\varepsilon_k - \mu)} + 1}.
\end{align*}
Sie entspricht der mittleren Besetzungszahl des Zustands $\ket{k}$.
Der Druck ergibt sich mittels
\begin{align*}
	p &= \frac{\kB T}{\lambda^3} f_{5/2}(z)
	\; , \quad \text{mit } 
	f_\nu(z)
	= \frac{4}{\sqrt{\pi}} \int_0^\infty \diff x\ x^2 \log\left[ 1 + z \mathrm{e}^{-x^2} \right]
	= \sum_{\ell = 1}^{\infty} \frac{(-1)^{\ell + 1} z^\ell}{\ell^{\nu}}.
\end{align*}

\emph{Beweis:}
\begin{align*}
	& \int \frac{\diff^3 k}{(2 \pi)^3} \log \left[ 1 +  z \mathrm{e}^{-\beta \varepsilon_k} \right] \\
	={}& \int \frac{\diff \Omega}{(2 \pi)^3} \int_0^\infty \diff k\ k^2 \log \left[ 1 +  z \mathrm{e}^{-\beta \frac{\hbar^2 k^2}{2 m}} \right]
\intertext{Substituiere $x = \sqrt{\frac{\beta \hbar^2}{2 m}} k$}
	={}& \frac{1}{2 \pi^2} \int_0^\infty \diff x\ \frac{1}{\left( \beta \frac{\hbar^2}{2 m} \right)^3} x^2 \log \left[ 1 +  z \mathrm{e}^{-x^2} \right].
	\qquad \blacksquare
\end{align*}

\begin{align*}
	\frac{N}{V} &= n = \frac{1}{\lambda^3} f_{3/2}(z) \; , \quad \text{mit } f_{3/2} = z \partial_z f_{5/2}(z) = \sum_{\ell = 1}^{\infty} \frac{(-1)^{\ell + 1} z^\ell}{\ell^{3/2}}.
\end{align*}

Nun betrachten wir zwei interessante Grenzfälle:
\begin{itemize}
	\item $n \lambda^3 \ll 1$ (Hohe Temperatur)
	\item $n \lambda^3 \gg 1$ (Tiefe Temperatur)
\end{itemize}

Hochtemperatur-Limes:
\[ 1 \gg n \lambda^3 = f_{3/2}(z) \]
Folglich ist $z$ klein
\[ z \ll 1 \]
Weil $z = \mathrm{e}^{\beta \mu}$ folgt
\[ \mu \to - \infty. \]
Für kleine Werte von $z$ gilt
\begin{align*}
	f_{5/2}(z) &= z - \frac{1}{\sqrt{8}} z^2 + \mathcal{O}(z^3) \\
	f_{3/2}(z) &= z - \frac{z^2}{2^{3/2}} + \mathcal{O}(z^3) \approx n \lambda^3 \\
	\leadsto z &= n \lambda^3 + \frac{1}{2 \sqrt{2}} (n \lambda^3)^2,
\end{align*}
damit
\begin{align*}
	p V
	&= \frac{\kB T}{\lambda^3} V f_{5/2}(z) \\
	&= \frac{\kB T}{\lambda^3} V \left[ z - \frac{1}{2^{5/2}} z^2 \right] \\
	&= \frac{\kB T}{\lambda^3} V \left[ n \lambda^3 + (n \lambda^3)^2 \left( \frac{1}{2\sqrt{2}} - \frac{1}{2^{5/2}} \right) \right] \\
	&= \kB T N \left[ 1 + (n \lambda^3) \frac{1}{2^{5/2}} + \ldots \right].
\end{align*}
Dies sind also die quantenmechanischen Korrekturen zum klassischen idealen Gasgesetz $p V = \kB T N$.
