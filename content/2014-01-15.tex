% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 15.01.2014
% TeX: Henri

\renewcommand{\printfile}{2014-01-15}

\begin{notice}[Erinnerung:]
	Mikrokanonisches Ensemble.
	%
	\begin{itemize}
		\item abgeschlossenes System
		\item $\displaystyle \varrho = \frac{1}{N! h^{3N}}$
		\item Das Volumen im Zustandsraum lautet: \[\displaystyle \Gamma(E) = \int_{E<H(p,q)<E+\Delta} \diff^{3N}p\diff^{3N}q\ \varrho(p,q)\]
		\item $S(E=U,N,V) = \kB \log \Gamma(E)$
	\end{itemize}
\end{notice}

\section{Kanonisches Ensemble} \index{Ensemble! klassisch kanonisch}

Sei nun ein Subsystem $(E_1,N_1,V_1)$ in ein abgeschlossenes System $(E_2,N_2,V_2)$ eingebettet. Zwischen den Systemen kann nun Energie ausgetauscht werden. Da das gesamte System abgeschlossen ist gilt weiterhin
%
\begin{align*}
	E &= E_1 + E_2 \\
	N &= N_1 + N_2 \\
	V &= V_1 + V_2
\end{align*}

Daher ist die Thermodynamik durch das mikrokanonische Ensemble beschrieben.

Wir verteilen die Energien
%
\begin{itemize}
	\item $E_2 = E - E_1$
	\item $E_1$
\end{itemize}
%
Damit können wir die Verteilungsfunktion aufschreiben
\begin{gather*}
	\varrho(p_1,q_1,E_1) \sim \Gamma_2(E-E_1) \\
	\begin{aligned}
		\int \diff p_1 \diff p_2 \diff q_1 \diff q_2 \frac{1}{N! h^{3N}}
		&= \int_{E_1} \diff p_1 \diff q_1 \underbrace{\left( \int_{E-E_1} \diff p_2 \diff q_2 \frac{1}{N! h^{3N}} \right)}_{\Gamma_2(E-E_1)}
	\end{aligned}
\end{gather*}

Da $E \gg E_1$ ist entwickeln wir $\Gamma_2(E-E_1)$ in $E_1$:
%
\begin{align*}
	\Gamma_2(E-E_1)
	&= \exp\left\{ \log \Gamma_2(E-E_1) \right\} \\
	&= \exp\left\{ S_2(E-E_1)/\kB \right\} \\
	&= \exp\left\{ \frac{1}{\kB} \left( S_2(E) - \frac{\partial S_2}{\partial E} E_1 \right) \right\} \\
	&= \exp\left\{ \frac{S_2(E)}{\kB} \right\} \exp\left\{ - \frac{E_1}{\kB T} \right\} \\
	&= \const \cdot \exp\left\{ - \frac{E_1}{\kB T} \right\} \\
	&= \const \cdot \exp\left\{ - \frac{H(p_1,q_1)}{\kB T} \right\}
\end{align*}

Wir definieren die Wahrscheinlichkeitsverteilung (die Dichtefunktion) für ein System angekoppelt an ein großes Reservoir als
%
\begin{align*}
	\varrho(p,q) &= \frac{1}{h^{3N} N!} \mathrm{e}^{-\frac{H(p,q)}{\kB T}}
\end{align*}

Wir definieren die Zustandssumme als
%
\begin{align*}
	Z_{N}(V,T)
	&= \int \diff^{3N}p \diff^{3N}q\ \varrho(p,q) \\
	&= \int \diff^{3N}p \diff^{3N}q\ \frac{1}{h^{3N} N!} \mathrm{e}^{-\frac{H(p,q)}{\kB T}}
\end{align*}
%
Die Zustandssume ergibt uns die freie Energie mittels
\[ F(V,N,T) = - \kB T \log Z_N \]
Dies liefert uns ein thermodynamisches Potential.

\begin{notice}[Vergleich mit dem mikrokanonischen Ensemble:]
	Statt Zustandssumme wurde hier das Phasenraumvolumen definiert
	%
	\begin{align*}
		\Gamma(E)
		&= \int_{E<H(p,q)<E+\Delta} \diff^{3N}p \diff^{3N}q\ \varrho(p,q)
	\end{align*}

	Die Entropie im mikrokanonischen Ensemble ist gegeben durch
	%
	\begin{align*}
		S(U,V,N) = \kB \log \Gamma(E)
	\end{align*}
\end{notice}

\begin{proof}[Beweis der Relation $F(V,N,T) = - \kB T \log Z_N$]
	\begin{align*}
		1
		&= \frac{1}{Z_N} \int \diff^{3N}p \diff^{3N}q\ \frac{1}{h^{3N} N!} \mathrm{e}^{-\frac{H(p,q)}{\kB T}} \\
		&= \mathrm{e}^{\beta F} \int \diff^{3N}p \diff^{3N}q\ \frac{1}{h^{3N} N!} \mathrm{e}^{-\beta H(p,q)}
		\intertext{Ableiten dieser Identität nach $\beta$}
		0
		&= \int \diff^{3N}p \diff^{3N}q\ \frac{1}{h^{3N} N!} \mathrm{e}^{\beta F} \mathrm{e}^{-\beta H} \left[ F + \beta \frac{\partial F}{\partial \beta} - H \right] \\
		&= \int \diff^{3N}p \diff^{3N}q\ \frac{1}{h^{3N} N!} \frac{1}{Z_N} \mathrm{e}^{-\beta H} \left[ F + \beta \frac{\partial F}{\partial \beta} - H \right] \\
		&= F + \beta \frac{\partial F}{\partial \beta} - \underbrace{\frac{1}{Z_N} \int \diff^{3N}p \diff^{3N}q\ \frac{1}{h^{3N} N!} H(p,q) \mathrm{e}^{-\beta H}}_{\braket{H} \equiv U} \\
		&= F + \beta \frac{\partial F}{\partial \beta} - U = 0 \\
		U &= F + \beta \frac{\partial F}{\partial \beta} = F + T \frac{\partial F}{\partial T}
	\end{align*}
	%
	Legendre-Transformation von $F$ ergibt innere Energie.
\end{proof}

\begin{align*}
	S &= - \left.\frac{\partial F}{\partial T}\right|_{V,N} \\
	U &= F + TS \; , \quad \text{kalorische Zustandsgleichung} \\
	p &= - \left.\frac{\partial F}{\partial V}\right|_{T,N} \; , \quad \text{thermische Zustandsgleichung} \\
	\mu &= \left.\frac{\partial F}{\partial N}\right|_{T,V} \\
\end{align*}

Für das ideale Gas gilt
%
\begin{align*}
	H(p,q) &= \sum_i \frac{p_i^2}{2 m} \\
	Z_N &= \underbrace{\int \diff^{3N}q}_{V^N} \diff^{3N}p\ \frac{1}{h^{3N} N!} \prod_i \mathrm{e}^{-\beta \frac{p_i^2}{2 m}}
\end{align*}

\section{Großkanonisches Ensemble} \index{Ensemble!klassisch großkanonisch}

Ein System (1) sei gekoppelt an ein Teilchenreservoir (2), mit dem Teilchen ausgetauscht werden können. Reservoir und System seien eingebettet in ein Wärmebad der Temperatur $T$.

Wir suchen eine Dichtefunktion (Wahrscheinlichkeitsverteilung) für das System (1) mit Teilchen-Austausch. Dabei ist System (1) $\ll$ (2).
%
\begin{align*}
	Z_N
	&= \int \frac{\diff p \diff q}{h^{3N} N!} \mathrm{e}^{-\beta H}
\intertext{Summiere über alle Aufteilungen mit $N_1$ Teilchen in (1) und $N-N_1$ Teilchen in (2)}
	&= \sum_{N_1 = 0}^{N} \frac{N!}{N_1! (N-N_1)!} \frac{1}{N!} \int \frac{\diff^{3N_1}p_1\diff^{3N_1}q_1}{h^{3N_1}} \int \frac{\diff^{3N_2}p_2\diff^{3N_2}q_2}{h^{3N_1}} \mathrm{e}^{-\beta H_1(p_1,q_1)} \mathrm{e}^{-\beta H_2(p_2,q_2)}
	\intertext{\textit{Bemerkung:} Vorraussetzung von schwacher Wechselwikrung zwischen den Systemen $H \simeq H_1 + H_2$ \hfill $\multimap$}
	&= \sum_{N_1 = 0}^{N} \int \diff^{3N_1}p_1\diff^{3N_1}q_1 \varrho(q_1,p_1,N_1) Z_N \\
	%
	\varrho(q_1,p_1,N_1) &= \frac{1}{Z_N} \frac{\mathrm{e}^{-\beta H_1(q_1,p_1)}}{h^{3N_1} N_1!} \int \frac{\diff p_2\diff q_2}{h^{3N_2} N_2!} \mathrm{e}^{-\beta H_2(q_2,p_2)} = \frac{1}{Z_N} \frac{\mathrm{e}^{-\beta H_1(q_1,p_1)}}{h^{3N_1} N_1!} Z_{N_2}(T,V_2) \\
	&= \frac{Z_{N_2}}{Z_N} \frac{\mathrm{e}^{-\beta H_1}}{h^{3N_1} N_1!} \\
	%
	\frac{Z_{N_2}}{Z_N}
	&= \frac{\mathrm{e}^{-\beta F(T,V_2,N_2)}}{\mathrm{e}^{-\beta F(T,V,N)}} \\
	&= \mathrm{e}^{-\beta [ F(T,V-V_1,N-N_1) - F(t,V,N) ]} \\
	&= \mathrm{e}^{-\beta[pv_1 - \mu N_1]} \\
	&= \mathrm{e}^{-\beta p V_1} z^{N_1} \; , \quad z = \mathrm{e}^{\beta \mu} \text{ Fugazität}
\intertext{Wir führen die Zustandssumme ein}
	\mathcal{Z}(T,V,z)
	&= \sum_{N=0}^{\infty} z^N Z_N(T,V) \\
	&= \sum_{N=0}^{\infty} \int \frac{\diff^{3N}p\diff^{3N}q}{h^{3N} N!} \mathrm{e}^{-\beta (H(p,q) - \mu N)}
\end{align*}
