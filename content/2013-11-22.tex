% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 22.11.2013
% TeX: Henri

\renewcommand{\printfile}{2013-11-22}

\section{van der Waals Gas} \index{van der Waals Gas}

Die Zustandsgleichung einer Substanz erhält man entweder aus dem Experiment oder durch ein kinetisches bzw.\ statistisches Modell. Die Hauptsätze selbst liefern uns keine näheren Aussagen über die Zustandsgleichung.

Mikrostandpunkt vom idealen Gas: Die Moleküle üben untereinander keine Wechselwirkung aus. Allerdings, zwischen Molekülen besteht Wechselwirkung mit abstoßendem und anziehendem Anteil.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (4,0) node[below] {$p$};
		\draw[->] (0,-1) -- (0,2) node[right] {$E_\textrm{pot}$};
		\draw[MidnightBlue] plot[smooth,samples=51,domain=0.85:3.5] (\x,{(\x)^(-12) - 2 * (\x)^(-6)}) node[below] {$\frac{1}{r^6}$};
	\end{tikzpicture}
	\caption{Typisches Wechselwirkungspotential.}
	\label{fig:2013-11-22-1}
\end{figure}

Bei höheren Dichten muss diese Wechselwirkung berücksichtigt werden.

\paragraph{van~der~Waals idealisierte Situation:}
%
\begin{itemize}
	\item Der abstoßende Teil des Potentials wird durch unendlich harte Kugeln modelliert.

	\item Der andere Teil des Potentials ist für die Anziehung verantwortlich.
\end{itemize}

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (4,0) node[below] {$p$};
		\draw[->] (0,-1) -- (0,2) node[right] {$E_\textrm{pot}$};
		\draw[MidnightBlue] (1,-1) -- (1,2);
		\draw[<->] (0,1) -- node[above] {$2r$} (1,1);
		\draw[MidnightBlue] plot[smooth,samples=51,domain=1:3.5] (\x,{(\x)^(-12) - 2 * (\x)^(-6)});
	\end{tikzpicture}
	\caption{van der Waals Vorschlag für ein Wechselwirkungspotential.}
	\label{fig:2013-11-22-2}
\end{figure}

\paragraph{Hypothese von van der Waals:} Die modifizierten Größen $v_\textrm{eff}$ und $p_\textrm{kin}$ erfüllen wieder die ideale Gasgleichung.
\[ v_\textrm{eff} p_\textrm{kin} = R T \]

\paragraph{Modell der harten Kugeln:} Die Anwesenheit eines anderen Moleküls in einem gewissen Raumbereich um das Molekül ist verboten.

Das zur Verfügung stehende Volumen $v_\textrm{eff}$ um $b$ ist kleiner als das tatsächliche Volumen $v$, wobei $b$ vom Molekülradius und der Anzahl der Moleküle pro Mol abhängt und für jeden Stoff eine charakteristische Konstante ist.
\[ v_\textrm{eff} = v - b \]

\paragraph{Tatsache der Anziehung:} Wegen der Anziehung werden die Moleküle der Oberfläche ins Innere gezogen. Der auf die Wand wirkende Druck $p$ ist kleiner als der durch die kinetische Energie der Moleküle bedingte Druck $p_\textrm{kin}$. Die Druckverringerung ist umso größer, je mehr Moleküle pro \si{\cubic\centi\m} im Inneren sind und je mehr Moleküle sich in der Randschicht befinden.
\[ \Delta p = \frac{a}{v^2} \]
wobei $a$ eine weitere charakteristische Konstante des Stoffes ist. Der Druck auf die äußere Wand ist folglich
\[ p = \textrm{kin} - \frac{a}{v^2} \]
Damit ergibt sich die \acct{van der Waals'sche Zustandsgleichung}
\[ \boxed{(v-b)\left( p + \frac{a}{v^2} \right) = RT} \]
Die stellt einen Näherungsausdruck für ein reales Gas dar.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\coordinate (O) at (0,0);
		\draw[->] (-0.3,0) -- (4,0) node[below] {$v$};
		\draw[->] (0,-0.3) -- (0,3.2) node[left] {$p$};
		\node[MidnightBlue,dot] (TC) at (2,2) {};
		\draw[dotted] (TC -| O) node[left] {$p_c$} -- (TC) -- (TC |- O) node[below] {$v_c$};
		\draw (0.6,3) .. controls (1,2.6) .. (2,2.5) .. controls (3,2.4) .. (3.6,1);
		\draw[MidnightBlue] (0.5,3) .. controls (1,2) .. (TC) .. controls (3,2) .. (3.5,0.5) node[right] {$T_c$};
		\draw (0.4,3) .. controls (1.2,1) .. (2,1.5) .. controls (2.8,2) .. (3.4,0.3);
		\draw (0.3,3) .. controls (1.2,0.5) .. (2,1) .. controls (2.8,1.5) .. (3.3,0.1);
	\end{tikzpicture}
	\caption{Im $p,V$-Diagramm werden die Isothermen gezeigt. Bei $T_c$ verschwinden die Minima/Maxima der Kurven.}
	\label{fig:2013-11-22-3}
\end{figure}

\subsection{Universelles Gasgesetz}

Für $T > T_c$ ist $p(v)$ monoton. Unterhalb von $T_c$ und bei fixem Druck $p < p_c$ erhalten wir drei Lösungen für $v$. Die kritischen Werte für $T_c,p_c,v_c$ folgen aus der Gleichung des van der Waals'schen Gases
%
\begin{align*}
	p &= P_\textrm{vdW}(v,T) = \frac{RT}{v-b}-\frac{a}{v^2} \\
	\frac{\partial p}{\partial v} &= 0 \to \frac{RT}{(v-b)^2} = \frac{2a}{v^3} \\
	\frac{\partial^2 p}{\partial v^2} &= 0 \to \frac{RT}{(v-b)^3} = \frac{3a}{v^4}
\end{align*}
%
nach einigen Umformungen ergibt sich
%
\begin{align*}
	p_c = \frac{1}{27} \frac{a}{b^2} \; , \quad v_c = 3b \; , \quad T_c = \frac{8}{27} \frac{a}{bR}
\end{align*}
%
Gehen wir zu normierten Variablen über
%
\begin{align*}
	\pi = \frac{p}{p_c} \; , \quad \nu = \frac{v}{v_c} \; , \quad t = \frac{T}{T_c}
\end{align*}
%
erhalten wir das van der Waals Gesetz der korrespondierenden Zustände (universelles Gasgesetz).
%
\begin{align*}
	\boxed{\left( \pi + \frac{3}{\nu^2} \right) \left( 3 \nu - 1 \right) = 8 t}
\end{align*}

Die Materialeigenschaften des Gases kommen in der reduzierten Form der Zustandsgleichung nicht mehr vor. Mit der kritischen Größen können wir das Verhältnis \[ \frac{RT_c}{p_c v_c} = \frac{8}{3} \] bilden.

\subsection{Maxwell-Konstruktion}

Für $T < T_c$ weisen die Isothermen steigende Stücke mit $\partial_v p|_T > 0$ auf. Die Stabilitätsbedingung $\kappa_T > 0$ ist somit verletzt. Die freie Energie mit $\partial_v f|_T = -p$ ist nicht konvex in $v$.

\paragraph{Erwartung:} Phasenübergang im Gebiet $T < T_c$. Wir müssen also unsere van der Waals Isothermen korrigieren.
Wir betrachten also eine Isotherme $T < T_c$ und schneiden sie mit einer Isobaren $p < p_c$.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\coordinate (O) at (0,0);
		\draw[->] (-0.3,0) -- (4,0) node[below] {$v$};
		\draw[->] (0,-0.3) -- (0,3.2) node[left] {$p$};
		\begin{scope}[every path/.style={}]
			\path[clip] (0,0) rectangle (2,1.5);
			\fill[pattern=north east lines,pattern color=DimGray] (0.4,3) .. controls (1.2,1) .. (2,1.5) -- cycle;
		\end{scope}
		\begin{scope}[every path/.style={}]
			\path[clip] (2,1.5) rectangle (4,4);
			\fill[pattern=north east lines,pattern color=DimGray] (2,1.5) .. controls (2.8,2) .. (3.4,0.3) -- cycle;
		\end{scope}
		\draw[DarkOrange3,name path=line] (0,1.5) -- (4,1.5);
		\draw[MidnightBlue,name path=curveleft] (0.4,3) .. controls (1.2,1) .. (2,1.5);
		\draw[MidnightBlue,name path=curveright] (2,1.5) .. controls (2.8,2) .. (3.4,0.3);
		\path[name intersections={of=line  and curveleft,by={a}}] node[dot,label={above right:$A$}] at (a) {};
		\path[name intersections={of=line  and curveright,by={b}}] node[dot,label={above right:$B$}] at (b) {};
		\draw[dotted] (b) -- (b |- O) node[below] {$v_A$};
		\draw[dotted] (a) -- (a |- O) node[below] {$v_B$};
		\node at (2,3) {$T < T_c$};
	\end{tikzpicture}
	\caption{Korrektur der van der Waals Isothermen.}
	\label{fig:2013-11-22-4}
\end{figure}

Wir suchen $p$ derart, dass $\mu_A(p,T) = \mu_B(p,T)$ mit einem Phasenübergang erster Ordnung von $A$ nach $B$. Mit $\mu = u - T s + p v$ erhalten wir
%
\begin{align}
	u_B - u_A - T(s_B - s_A) + p(v_B - v_A) = 0 \tag{$\square$}\label{eq:2013-11-22-stern1}
\end{align}
%
und muss erfüllt werden.

Wir benötigen $u$ und $s$. Für ein reales Gas wissen wir
%
\begin{align*}
	\left. \frac{\partial u}{\partial v} \right|_T = T \left. \frac{\partial p}{\partial T} \right|_v - p
	\; , \quad
	p_\textrm{vdW} = \frac{RT}{v-b}-\frac{a}{v^2}
\end{align*}
%
mit dem Ausdruck für $p_\textrm{vdW}$:
%
\begin{align}
	\left. \frac{\partial u}{\partial v} \right|_T = \frac{a}{v^2} \tag{$\ast$}\label{eq:2013-11-22-stern2}
\end{align}
%
als Folge der Attraktion der Moleküle.

Für $u_B - u_A$ bei $T_A = T_B$ gilt
%
\begin{align*}
	u_B - u_A = \int_{A}^{B} \diff v \frac{a}{v^2} = - \frac{a}{v_B} + \frac{a}{v_A}
\end{align*}

Mit Hilfe des ersten Hauptsatzes könne wir für $S$ angeben
%
\begin{align*}
	\diff s
	&= \frac{1}{T}(\diff u + p \diff v)
	= \frac{1}{T} \left(
		\left. \frac{\partial u}{\partial T} \right|_v \diff T
		+ \left. \frac{\partial u}{\partial v} \right|_T \diff v
	\right)
	+ \frac{p}{T} \diff v \\
	&= \frac{1}{T} \left(
		\frac{a}{v^2} + \left\{ \frac{RT}{v-b} - \frac{a}{v^2} \right\}
	\right) \diff v
	= \frac{R}{v-b} \diff v \\
	\implies s_B - s_A &= R \ln\left( \frac{v_B - b}{v_A - b} \right)
\end{align*}
%
Alles einsetzen in \eqref{eq:2013-11-22-stern1}
%
\begin{align*}
	p(v_B - v_A)
	&= \frac{a}{v_B} + RT \ln(v_B - b) - \left( \frac{a}{v_A} + RT \ln(v_A - b) \right) \tag{$\ast\ast$}\label{eq:2013-11-22-stern3}
\end{align*}

Wird andererseits die Fläche direkt mit $p_\textrm{vdW}$ berechnet, finden wir
%
\begin{align*}
	\int_{A}^{B} p_\textrm{vdW} \diff v
	= RT \int_{A}^{B} \frac{\diff v}{v-b} - \int_{A}^{B} \frac{a \diff v}{v^2}
	= RT \ln(v-b) |_{A}^{B} + \left. \frac{a}{v} \right|_{A}^{B}
\end{align*}
%
was mit der rechten Seite von \eqref{eq:2013-11-22-stern3} übereinstimmt, so dass
%
\begin{align*}
	\int_{A}^{B} p \diff v = p(v_B - v_A)
\end{align*}
%
Die Gleichgewichtsbedingung $\mu_A = \mu_B$ reduziert sich auf die Gleichheit der Flächen in Abbildung~\ref{fig:2013-11-22-4}.

Die \acct{Maxwell-Konstruktion} zur Korrektur der van der Waals Isothermen führt zu thermodynamisch konsistenten (stabilen) Isothermen. Die korrigierte Isotherme beim Phasenübergang erster Ordnung von flüssig ($A$) zu gasförmig ($B$).

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\coordinate (O) at (0,0);
		\draw[->] (-0.3,0) -- (4,0) node[below] {$v$};
		\draw[->] (0,-0.3) -- (0,3.2) node[left] {$p$};
		\begin{scope}[every path/.style={}]
			\path[clip] (0,0) rectangle (2,1.5);
			\fill[pattern=north east lines,pattern color=DimGray] (0.4,3) .. controls (1.2,1) .. (2,1.5) -- cycle;
		\end{scope}
		\begin{scope}[every path/.style={}]
			\path[clip] (2,1.5) rectangle (4,4);
			\fill[pattern=north east lines,pattern color=DimGray] (2,1.5) .. controls (2.8,2) .. (3.4,0.3) -- cycle;
		\end{scope}
		\draw[DarkOrange3,name path=line] (0,1.5) -- (4,1.5);
		\draw[MidnightBlue,name path=curveleft] (0.4,3) .. controls (1.2,1) .. node[left,pos=0.5] {$\alpha'$} node[dot,label={below:$B'$},pos=0.67] {} (2,1.5);
		\draw[MidnightBlue,name path=curveright] (2,1.5) .. controls (2.8,2) .. node[right,pos=0.5] {$\beta'$} node[dot,label={above:$A'$},pos=0.33] {} (3.4,0.3);
		\path[name intersections={of=line  and curveleft,by={a}}] node[label={above right:$A$}] at (a) {};
		\path[name intersections={of=line  and curveright,by={b}}] node[label={below left:$B$}] at (b) {};
		\node at (2,3) {$T < T_c$};
	\end{tikzpicture}
	\caption{Korrigierte van der Waals Isotherme.}
	\label{fig:2013-11-22-5}
\end{figure}

Die Punkte $A'$ und $B'$ definieren die Spinodale, Endpunkte der (meta)stabilen Phasen.

Segmente:
%
\begin{itemize}
	\item $\alpha'$: Überhitzte Flüssigkeit (Siedeverzug)
	\item $\beta'$: Unterkühltes Gas (Übersättgung)
\end{itemize}

\begin{notice}[Beachte:]
	Für $T > T_c$ ist eine Verflüssigung des Gases durch Druckerhöhung unmöglich. Gas und Flüssigkeit sind oberhalb von $T_c$ ununterscheidbare Phasen.
\end{notice}
