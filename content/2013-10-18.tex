% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 18.10.2013
% TeX: Henri

\renewcommand{\printfile}{2013-10-18}

In der Festkörperphysik spielt auch die \acct{Isotropie} eine Rolle. Das bedeutet, dass die Teilchen gleichverteilt sind und es keine Vorzugsrichtung gibt.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw (1,-1) -- (1,2);
		\draw (2,-1) -- (2,2);
		\draw (0,0) -- (1,0) node[dot] (A) {} -- (2,0) node[dot] (B) {} -- (3,0);
		\draw (0,1) -- (1,1) node[dot] (C) {} -- (2,1) node[dot] (D) {} -- (3,1);
		\draw (C) edge[->,bend left] node[above] {$t$} (D);
		\draw (C) edge[->,bend right] node[left] {$t$} (A);
	\end{tikzpicture}
	\caption{Zustände gehen mit Austauschraten $t$ ineinander über.}
	\label{fig:2013-10-18-1}
\end{figure}

Damit ein Prozess isotherm oder isobar sein kann wird ein Reservoir benötigt. Um die Temperatur konstant zu halten wird ein Wärmereservoir benötigt, welches beliebig viel Wärme zu- bzw.\ abführt.

Es gibt ebenso Teilchen- oder Druckreservoirs.

\section{Wärme und Arbeit}

Der Begriff der Arbeit ist bereits aus der klassischen Mechanik bekannt und lautet
%
\begin{equation*}
	\delta W = F \diff x
\end{equation*}

Der Druck $p$ ist die Kraft pro Fläche und somit
%
\begin{equation*}
	\delta W = p \cdot A \diff x = p \diff V
\end{equation*}
%
Das $\delta$ signalisiert, dass es sich nicht um ein vollständiges Differential handelt.

Die auf ein System übertragene Wärmemenge ist
%
\begin{equation*}
	\delta Q = C \diff T
\end{equation*}
%
wobei $C$ die spezifische Wärme darstellt. Wird die spezifische Wärme pro Mol eines Stoffes angegeben, so schreibt man $c$.

\section{Einheiten}

Temperaturen werden in \si{\kelvin} oder \si{\degreeCelsius} gemessen. Bei $T = \SI{0}{\degreeCelsius}$ gefriert Wasser, $T = \SI{0}{\kelvin}$ ist der absolute Nullpunkt.

Der Druck $p$ wird in \si{torr}, \si{atm}, \si{\bar} oder \si{\pascal} gemessen. Die Umrechnungen sind
%
\begin{align*}
	\SI{1}{atm} &= \SI{760}{torr} \\
	\SI{1}{atm} &= \SI{9.81e4}{\N\per\square\m} = \SI{0.981}{\bar} \\
	\SI{1}{\bar} &= \SI{e5}{\N\per\square\m} = \SI{e5}{\pascal}
\end{align*}

Die Wärmemenge $\delta Q$ wird in \si{cal}, \si{kcal}, \si{\joule}, \si{\watt\second} oder \si{erg} gemessen. Dabei gilt
%
\begin{align*}
	\SI{1}{kcal} &= \SI{4187}{\joule} \\
	\SI{1}{\joule} &= \SI{1}{\watt\second} = \SI{e7}{erg}
\end{align*}

Das Maß für die Sotffmenge $N$ ist \si{\mol} oder \si{\g}. Dabei entspricht $\SI{1}{\mol} = \SI{6.022e23}{Moleküle}$. Die Normierung findet über das Kohlenstoffatom statt mit $\SI{1}{\mol} {}^{12}\mathrm{C} = \SI{12}{\g}$.

\section{Materialkoeffizienten}

Materialkoeffizienten sind oft benutzte Verknüpfungen zwischen $p,V,T$. Sie dienen der Charakterisierung von Medien.
%
\begin{itemize}
	\item Ausdehnungskoeffizienten \index{Ausdehnungskoeffizient}
		\[
			\alpha = \frac{1}{V} \left. \frac{\partial V}{\partial T} \right|_p
		\]

	\item Spannungskoeffizienten \index{Spannungskoeffizient}
		\[
			\beta = \frac{1}{p} \left. \frac{\partial p}{\partial T} \right|_V
		\]

	\item (isotherme) Kompressibilität \index{Kompressibilität !isotherm}
		\[
			\kappa_T = - \frac{1}{V} \left. \frac{\partial V}{\partial p} \right|_T
		\]

	\item (adiabatische) Kompressibilität \index{Kompressibilität !adiabatisch}
		\[
			\kappa_S = - \frac{1}{V} \left. \frac{\partial V}{\partial p} \right|_S
		\]
\end{itemize}

\chapter{Zum idealen Gas}

Ein reales Gas approximiert ein ideales Gas umso besser, je tiefer sein Siedepunkt ist. Bei \SI{760}{Torr} findet man die in Tabelle~\ref{tab:2013-10-18-1} aufgetragenen Werte.

\begin{table}
	\centering
	\begin{tabular}{cccccc}
		\toprule
		Gas & $\mathrm{He}$ & $\mathrm{H_2}$ & $\mathrm{N_2}$ & $\mathrm{O_2}$ & $\mathrm{H_2O}$ \\
		$t_S$ [\si{\degreeCelsius}] & \num{-269} & \num{-253} & \num{-196} & \num{-183} & \num{100} \\
		\bottomrule
	\end{tabular}
	\caption{Siedetemperaturen verschiedener Gase.}
	\label{tab:2013-10-18-1}
\end{table}

Nach \acct*{Boyle-Mariotte} \index{Gesetz von!Boyle-Mariotte} gilt
%
\begin{equation*}
	p V|_T = \const
\end{equation*}
%
und \acct*{Gay-Lussac} \index{Gesetz von!Gay-Lussac} findet
%
\begin{equation*}
	p V = \const T
\end{equation*}
%
Damit ergibt sich für $\alpha,\beta,\kappa$
%
\begin{align*}
	\alpha &= \beta = \frac{1}{T} \\
	\kappa &= \frac{1}{p}
\end{align*}

Wird die Skaleneinteilung von $T$ an die Centigradeinteilung angepasst, so gilt
%
\begin{equation*}
	T = T_0 + t
\end{equation*}
%
wobei $t$ die Celsiustemperatur und $T_0 = \SI{273.15}{\degreeCelsius}$ sind.

Nach Avogadro gilt, dass alle Gase unter gleichen Bedingungen von Druck $p$ und Temperatur $T$ im gleichen Volumen $V$ die gleiche Anzahl an Molekülen enthalten.

Das Molvolumen ist definiert als
%
\begin{equation*}
	\left. V_{\si{\mol}} \right|_{p = \SI{760}{Torr}, t = \SI{0}{\degreeCelsius}} = \SI{22.4e-3}{\cubic\m} = \SI{22.4}{\cubic\deci\m}
\end{equation*}
%
Damit ergibt sich die Gaskonstante $R$ zu
%
\begin{equation*}
	p V_{\si{\mol}} = R T
\end{equation*}
%
mit $R = \SI{8.314}{\joule\per\degreeCelsius\per\mol}$.

Mit $n$ (Anzahl der Mole) erhalten wir die \acct{Zustandsgleichung des idealen Gases}
%
\begin{equation*}
	\boxed{p V = n R T}
\end{equation*}

\begin{notice}[Ergänzung]
	Gaskonstante pro Teilchen
	%
	\begin{equation*}
		\kB = \frac{R}{L} = \SI{1.381e-23}{\joule\per\degreeCelsius}
	\end{equation*}
	%
	mit $L = \SI{6.022e23}{\per\mol}$, der Lochschmidtschen Zahl.
\end{notice}

Die erlaubt das Einführen des spezifischen Volumens
%
\begin{align*}
	v &= \frac{V}{n} \\
	\implies p v &= R T
\end{align*}

\chapter{Der erste Hauptsatz der Thermodynamik}

Sei $\delta Q$ die dem System in einer beliebigen Zustandsänderung zugeführte Wärme und $\delta W$ die vom System geleistete Arbeit, dann ändert sich die im System enthaltene innere Energie $U$ um das vollständige Differential
%
\begin{equation*}
	\diff U = \delta Q - \delta W
\end{equation*}

Das heißt $\diff U$ ist unabhängig vom Weg der Zustandsänderung.
\[ \oint \diff U = 0 \] für jeden Kreisprozess.

Der erste Hauptsatz lässt sich auf die spezifischen Größen $u,q,v$ übertragen
\[ \diff u = \delta q - p \delta v \]

\section{Versuch von Gay-Lussac} \index{Versuch von!Gay-Lussac}

Die experimentelle Versuchsanordnung ist in Abbildung~\ref{fig:2013-10-18-2} zu sehen.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw (0,0) rectangle (4,2);
		\draw (1,1)+(10:0.8) coordinate (A) arc[start angle=10,end angle=350,radius=0.8] coordinate (B);
		\draw (3,1)+(170:0.8) coordinate (C) arc[start angle=170,end angle=-170,radius=0.8] coordinate (D);
		\draw (A) -- (C);
		\draw (B) -- (D);
		\draw ($(A)!0.5!(C) + (0,0.2)$) -- ($(B)!0.5!(D) + (0,-0.2)$);
		\node at (1,1) {$V_1$};
		\node at (3,1) {$V_2$};
		\node at (2,0.2) {$T$};
	\end{tikzpicture}
	\caption{Versuchsaufbau von Gay-Lussac.}
	\label{fig:2013-10-18-2}
\end{figure}

$T_1$ ist die Anfangstemperatur des Gases in $V_1$, $T_2$ ist die Endtemperatur des Gases in $V_1 + V_2$.

Das experimentelle Ergebnis ist, dass sich die Temperatur nicht ändert und $T_1 \approx T_2$. Für ein ideales Gas würde gelten $T_1 = T_2$.

Das Gas entzieht der Umgebung während der Expansion keine Wärme \[ \delta Q = 0 \] außerdem wird keine Arbeit verrichtet \[ \delta W = 0 \]

Es folgt, dass $U$ unabhängig von $V$ sein muss
%
\begin{align*}
	U_\textrm{ideales Gas} &= U(T) \\
	\partial_V U |_T &= 0
\end{align*}

Mit der Definition der spezifischen Wärme $\delta Q = C_V(T) \diff T$ folgt die \acct*{kalorischen Zustandsgleichung des idealen Gases}\index{kalorischen Zustandsgleichung!ideales Gas}
%
\begin{equation*}
	\boxed{U(T) = \int_{0}^{T} \diff T' C_V(T')}
\end{equation*}
%
Für ein monoatomares Gas ist $C_V$ eine Konstante \[ U(T) = C_V T \]
Für ein diatomiges Gas ist $C_V$ stückweise konstant.

\begin{notice}[Anwendung:]
	Betrachte zwei Zustandsänderungen bei $v = \const$ und $p = \const$.
	%
	\begin{itemize}
		\item $v = \const$:
			\begin{itemalign}
				\diff u_v &= \delta q = c_v \diff T \\
				[c_v &= \partial_T u|_v ]
			\end{itemalign}

		\item $p = \const$:
			\begin{itemalign}
				\diff u_p
				&= \delta q - \delta w \\
				&= c_p \diff T - p \diff v \\
				[ c_p &= \partial_T (u+pv) |_p ]
			\end{itemalign}
	\end{itemize}
	%
	Einsetzen von $p \diff v = R \diff T$ bei $p = \const$ liefert
	\[ \diff u_p = (c_p - R) \diff T = \diff u_v = c_v \diff T \]
	wobei die Temperaturänderung $\diff T$ in den Prozessen gleich ist.
	\[ \implies \boxed{c_p - c_v = R > 0} \]
	Die Relation $c_p > c_v$ ist eine Konsequenz aus der Tatsache, dass bei $p = \const$ zusätzliche Arbeit geleistet wird.
\end{notice}

Später wird gezeigt, dass \[ c_v = \frac{f}{2} R \] wobei $f$ die Anzahl der Freiheitsgrade ist.
Für ein monoatomiges Gas gilt $f=3$ (drei kinetische Freiheitsgrade), für leichte diatomige Moleküle $f=5$ und für schwere diatomige Moleküle $f=7$.

Für ein monoatomiges Gas gilt
%
\begin{equation*}
	u = \frac{3}{2} R T \; , \quad U = \frac{3}{2} n R T
\end{equation*}
