% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 20.12.2013
% TeX: Henri

\renewcommand{\printfile}{2013-12-20}

\subsection{Bernoullis Gesetz} \index{Bernoullis Gesetz}

Für eine ideale Flüssigkeit gelten die Euler Gleichungen im stationären Fall, also $\partial_t \bm{u} = 0$.
%
\begin{align*}
	m D_t \bm{u} = \bm{F} - \frac{\nabla p}{n}
\end{align*}

\begin{notice}
	\[
		\nabla (\bm{a} \cdot \bm{b}) = (\bm{a} \cdot \nabla) \bm{b} + (\bm{b} \cdot \nabla) \bm{a} + \bm{a} \wedge (\nabla \wedge \bm{b}) + \bm{b} \wedge (\nabla \wedge \bm{a})
	\]
	mit $\bm{a} = \bm{b} = \bm{u}$
	\[ \frac{1}{2} \nabla \bm{u}^2 = (\bm{u} \cdot \nabla) \bm{u} + \bm{u} \wedge (\nabla \wedge \bm{u}) \]
\end{notice}

\begin{align*}
	m (\bm{u} \cdot \nabla) \bm{u} &= \left[ \frac{1}{2} \nabla \bm{u}^2 - \bm{u} \wedge (\nabla \wedge \bm{u}) \right] m \\
	- m \bm{u} \wedge (\nabla \wedge \bm{u}) &= - \frac{m}{2} \nabla \bm{u}^2 - \nabla \phi - \frac{\nabla p}{n}
\end{align*}
%
\begin{itemize}
	\item Wirbelfreie Strömungen (keine Turbulenzen)
		\[ 0 = - \nabla \left( \frac{m}{2} \bm{u}^2 + \phi \right) - \frac{\nabla p}{n} \]
	\item Annahme von inkompressibler Flüssigkeit: $n = \const$
		\[ \aligned[t]
			0 &= - \nabla \left( \frac{m}{2} \bm{u}^2 + \phi + \frac{p}{n} \right) \\
			\const &= \frac{m}{2} \bm{u}^2 + \phi + \frac{p}{n}
		\endaligned \]
\end{itemize}

Bernoullis Gesetz sagt, dass je schneller die Flüssigkeit fließt, desto tiefer ist der Druck.

Für kompressible Flüssigkeiten
%
\begin{align*}
	\diff \left( \frac{h}{n} \right) &= T \underbrace{\diff \left( \frac{s}{n} \right)}_{=0} + \frac{\diff p}{n} = \frac{\diff p}{n}
	\implies \nabla \left( \frac{h}{n} \right) = \frac{\nabla p}{n} \\
	0 &= \nabla \left( \frac{m}{2} \bm{u}^2 + \phi + \frac{h}{n} \right) \; , \quad h = \frac{5}{2} n \kB T = \frac{5}{2} p \\
	\implies \const &= \frac{m}{2} \bm{u}^2 + \phi + \frac{5 p}{2 n}
\end{align*}

Für eine ideale Flüssigkeit
%
\begin{itemize}
	\item keine Wirbel $\nabla \wedge \bm{u} = 0$
	\item inkompressibel $n = \const$
	\item stationär $\partial_t \bm{u} = 0$
\end{itemize}
%
Dies liefert uns eine Kontinuitätsgleichung
\[ \cancel{$\partial_t n$} + \tikz[remember picture]{\coordinate (A1) at (0,1ex);} \nabla ( \tikz[remember picture,baseline=(B1.base),every path/.style={}]{\node[inner sep=0pt] (B1) {$n$};} \bm{u} ) = n \nabla \cdot \bm{u} = 0 \]
\begin{tikzpicture}[remember picture,overlay,every path/.style={}]
	\draw (B1) edge[out=100,in=80,->] (A1);
\end{tikzpicture}
Wegen $\nabla \wedge \bm{u} = 0$ können wir ein Potential einführen mit $\nabla \phi = - \bm{u}$ (Potentialströmung). Es folgt
\[ \nabla \cdot \bm{u} = \nabla^2 \phi = 0 \]

\subsection{Schallwellen}

\begin{align*}
	\partial_t n + \nabla (n \bm{u}) &= 0 \\
	\partial_t \bm{u} + (\bm{u} \cdot \nabla) \bm{u} &= - \frac{\nabla p}{n}
\end{align*}

\begin{align*}
	n \sim n_0 + n' \\
	p \sim p_0 + p' \\
	\bm{u} \text{ klein}
\end{align*}

Linearisieren der Gleichung
%
\begin{align*}
	\partial_t n' + n_0 \nabla \cdot \bm{u} &= 0 \\
	\partial_t \bm{u} &= - \frac{\nabla p'}{n_0} \\
	\nabla p' &= \underbrace{\left. \frac{\partial p'}{\partial n'} \right|_{s/n}}_{=\frac{1}{n_0 \kappa_s}} \nabla n'
\end{align*}
%
\begin{align*}
	\partial_t p' + n \left. \frac{\partial p'}{\partial n'} \right|_{s/n} \nabla \cdot \bm{u} &= 0 \\
	\partial_t \bm{u} + \frac{\nabla p}{n} &= 0
\end{align*}
%
Durch Fouriertransformation folgt
%
\begin{align*}
	- \mathrm{i} \omega p' + \frac{1}{\kappa_s} \mathrm{i} \bm{q} \cdot \bm{u} &= 0 \\
	- \mathrm{i} \omega \bm{u} + \mathrm{i} \bm{q} \frac{p'}{n} &= 0 \\
	\implies - \omega \bm{q} \cdot \bm{u} + q^2 \frac{p'}{n} &= 0
\end{align*}
%
\begin{align*}
	\begin{bmatrix}
		- \omega & \frac{1}{\kappa_s} \\
		\frac{q^2}{n} & - \omega \\
	\end{bmatrix}
	\begin{pmatrix}
		p \\ \bm{q} \cdot \bm{u} \\
	\end{pmatrix}
	&= 0 \\
	\det(\cdot) = 0 &= \omega^2 - \frac{1}{n_0 \kappa_s} q^2
\end{align*}
%
Lineare Schallgeschwindigkeit \[ c_s = \sqrt{\frac{1}{n_0 \kappa_s}} \]

\minisec{Viskoser Fluss zwischen zwei Platten}
Betrachte das eindimensionale Problem im stationären Zustand, also $u_y = u_z = 0$ und $n = \const$. Einsetzen
%
\begin{align*}
	\partial_x (n u_x) &= 0 \\
	\implies \partial_x u_x &= 0
\end{align*}
%
Mit Navier-Stokes
%
\begin{align*}
	D_t \bm{u} &= 0 \\
	\nabla p &\implies \partial_x p \\
	n \bm{F} &\implies F_x
\end{align*}
%
\begin{align*}
	[\hat{p}]_{ik}
	&= - \eta \left[ \partial_i u_k + \partial_k u_i - \frac{2}{3} \cancel{$\nabla \bm{u}$} \delta_{ik} \right]
	= - \eta
	\begin{bmatrix}
		& & \partial_z u_x \\
		& & \\
		\partial_z u_x & & \\
	\end{bmatrix} \\
	\nabla \hat{p} &= - \eta
	\begin{pmatrix}
		\partial_z^2 u_x \\
		\\
		\cancel{$\partial_x \partial_z u_x$} \\
	\end{pmatrix} \\
	\eta \partial_z^2 u_x &= F_x + \frac{\partial p}{\partial x} = a \\
	u_x(z) &= b + z c + \frac{a}{2 \eta} z^2 \\
	\implies u_x(z) &= \frac{a}{2 \eta} \left( z^2 - d^2 \right)
\end{align*}
%
Randbedingungen $u_x(d) = u_x(-d) = 0$.

Falls keine Kraft wirkt: $\bm{F} = 0$ so muss \[ \frac{\partial p}{\partial x} = a \]
Druckunterschied an den beiden Enden: \[ \Delta p = L q \]

